import mongoose from 'mongoose'
import { app } from './app'
import { natsWrapper } from './natsWrapper'

const start = async () => {
  console.log(`calendars service starting up on ${process.env.HOSTNAME}`)
  process.env.NATS_CLIENT_ID = process.env.HOSTNAME

  // Check environment variables
  if (!process.env.JWT_KEY) {
    throw new Error('Please define JWT_KEY environment variable')
  }

  if (!process.env.MONGO_URI) {
    throw new Error('Please define MONGO_URI environment variable')
  }

  if (!process.env.NATS_CLUSTER_ID) {
    throw new Error('Please define NATS_CLUSTER_ID environment variable')
  }

  if (!process.env.NATS_CLIENT_ID) {
    throw new Error('Please define NATS_CLIENT_ID environment variable')
  }

  if (!process.env.NATS_URL) {
    throw new Error('Please define NATS_URL environment variable')
  }

  try {
    await natsWrapper.connect(
      process.env.NATS_CLUSTER_ID,
      process.env.NATS_CLIENT_ID,
      process.env.NATS_URL
    )

    natsWrapper.client.on('close', () => {
      console.log('NATS connection closed')
      process.exit()
    })

    process.on('SIGINT', () => natsWrapper.client.close())
    process.on('SIGTERM', () => natsWrapper.client.close())

    // Event Bus Listeners

    // MongoDb
    const connectWithRetry = async () => {
      mongoose.connect(process.env.MONGO_URI!, {}, function (err) {
        if (err) {
          console.error(
            'Failed to connect to mongo on startup - retrying in 5 sec'
          )
          console.error(err)
          setTimeout(connectWithRetry, 5000)
        } else {
          console.log('Connected to MongoDb', process.env.MONGO_URI)
        }
      })
    }
    await connectWithRetry()
  } catch (error) {
    console.error(error)
    process.exit(1)
  }

  app.listen(5000, () => {
    console.log(`${process.env.HOSTNAME} listening on 5000`)
  })
}

start()
