import mongoose from 'mongoose'
import { updateIfCurrentPlugin } from 'mongoose-update-if-current'

interface CalendarAttr {
  groupId: string
  ownerId: string
  name: string
}

interface CalendarDoc extends mongoose.Document {
  groupId: string
  ownerId: string
  name: string
  version: number
}

interface CalendarModel extends mongoose.Model<CalendarDoc> {
  build(attrs: CalendarAttr): CalendarDoc
  findByGroup(
    id: string,
    currentUser: { groups: string[] }
  ): Promise<CalendarDoc | null>
}

const CalendarSchema = new mongoose.Schema(
  {
    groupId: {
      type: String,
      required: true,
    },
    ownerId: {
      type: String,
      required: true,
    },
    name: {
      type: String,
      required: true,
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id
        delete ret._id
      },
    },
  }
)

CalendarSchema.set('versionKey', 'version')
CalendarSchema.plugin(updateIfCurrentPlugin)

CalendarSchema.statics.findByGroup = (id, currentUser) => {
  return Calendar.findOne({
    _id: id,
    groupId: currentUser.groups[0],
  })
}
CalendarSchema.statics.build = (attrs: CalendarAttr) => {
  return new Calendar({
    groupId: attrs.groupId,
    ownerId: attrs.ownerId,
    name: attrs.name,
  })
}

const Calendar = mongoose.model<CalendarDoc, CalendarModel>(
  'Calendar',
  CalendarSchema
)

export { Calendar }
