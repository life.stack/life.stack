import mongoose, { Schema } from 'mongoose'
import { updateIfCurrentPlugin } from 'mongoose-update-if-current'

interface EventAttr {
  calendarId: string
  // mobiscroll attributes
  start: string
  end?: string
  title: string
  allDay?: boolean
  color?: any
  editable?: boolean
  resource?: string | [string]
  recurring?: string | object
  recurringException?: string | object | [string]
  recurringExceptionRule?: string | object
  // custom attributes
  location?: string
  url?: string
  notes?: string
}

// This Event Document at it's core is the mobiscroll event object
// https://docs.mobiscroll.com/react/eventcalendar#opt-data
interface EventDoc extends mongoose.Document {
  // app
  version: number
  calendarId: string
  groupId: string
  // mobiscroll attributes
  start: string
  end?: string
  title: string
  allDay?: boolean
  color?: string
  editable?: boolean
  resource?: string | [string]
  recurring?: string | object
  recurringException?: string | object | [string]
  recurringExceptionRule?: string | object
  // custom attributes
  location?: string
  url?: string
  notes?: string
}

interface EventModel extends mongoose.Model<EventDoc> {
  build(attrs: EventAttr): EventDoc
  findByGroup(
    id: string,
    currentUser: { groups: string[] }
  ): Promise<EventDoc | null>
}

const EventSchema = new mongoose.Schema(
  {
    calendarId: {
      type: mongoose.Types.ObjectId,
      ref: 'Calendar',
      required: true,
    },
    groupId: {
      type: String,
    },
    // mobi

    start: { type: String, required: true },
    end: String,
    title: String,
    allDay: Boolean,
    color: String,
    editable: Boolean,
    resource: {
      type: Schema.Types.Mixed,
    },
    recurring: {
      type: Schema.Types.Mixed,
    },
    recurringException: {
      type: Schema.Types.Mixed,
    },
    recurringExceptionRule: {
      type: Schema.Types.Mixed,
    },

    // custom
    location: String,
    url: String,
    notes: String,
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id
        delete ret._id
      },
    },
  }
)

EventSchema.set('versionKey', 'version')
EventSchema.plugin(updateIfCurrentPlugin)

EventSchema.statics.findByGroup = (id, currentUser) => {
  return Event.findOne({
    _id: id,
    groupId: currentUser.groups[0],
  })
}
EventSchema.statics.build = (attrs: EventAttr) => {
  return new Event({
    ...attrs,
  })
}

const Event = mongoose.model<EventDoc, EventModel>('Event', EventSchema)

export { Event }
