import express, { Request, Response } from 'express'
import { NotFoundError, requireAuth } from '@life.stack/common'
import { Calendar } from '../../models/calendar'

const getCalendarRouter = express.Router()

getCalendarRouter.get(
  '/api/calendars/:calendarId',
  requireAuth,
  async (req: Request, res: Response) => {
    const { calendarId } = req.params

    const calendar = await Calendar.findByGroup(calendarId, req.currentUser!)

    if (!calendar) {
      throw new NotFoundError()
    }

    res.status(200).send(calendar)
  }
)

export { getCalendarRouter }
