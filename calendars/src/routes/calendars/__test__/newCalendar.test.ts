import { app } from '../../../app'
import request from 'supertest'

const validCalendar = {
  name: 'Calendar',
}

it('has a route handler for POST /api/calendars', async () => {
  const response = await request(app).post('/api/calendars').send()

  expect(response.statusCode).not.toEqual(404)
})

it('requires authorization', async () => {
  await request(app).post('/api/calendars').send(validCalendar).expect(401)

  await request(app)
    .post('/api/calendars')
    .set('Cookie', global.signin())
    .send(validCalendar)
    .expect(201)
})
