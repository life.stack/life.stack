import request from 'supertest'
import { app } from '../../../app'

const setup = async () => {
  const user = global.signin()

  const { body: createdCalendar } = await request(app)
    .post('/api/calendars')
    .set('Cookie', user)
    .send({ name: 'test cal' })

  return { user, createdCalendar }
}

it('has a route handler for GET /api/calendars/:calendarId', async () => {
  const { user, createdCalendar } = await setup()

  await request(app)
    .get(`/api/calendars/${createdCalendar.id}`)
    .set('Cookie', user)
    .send()
    .expect(200)

  await request(app)
    .get(`/api/calendars/${createdCalendar.id}`)
    .send()
    .expect(401)
})
