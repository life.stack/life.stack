import express, { Request, Response } from 'express'
import { NotFoundError, requireAuth } from '@life.stack/common'
import { Calendar } from '../../models/calendar'
import { Event } from '../../models/event'

const removeEventRouter = express.Router()

removeEventRouter.delete(
  '/api/calendars/:calendarId/events/:eventId',
  requireAuth,
  async (req: Request, res: Response) => {
    const { calendarId, eventId } = req.params

    const calendar = await Calendar.findByGroup(calendarId, req.currentUser!)

    if (!calendar) {
      throw new NotFoundError()
    }

    const event = await Event.findByIdAndRemove(eventId)

    res.status(200).send(event)
  }
)

export { removeEventRouter }
