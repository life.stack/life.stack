import express, { Request, Response } from 'express'
import { NotFoundError, requireAuth } from '@life.stack/common'
import { Calendar } from '../../models/calendar'
import { Event } from '../../models/event'

const updateEventRouter = express.Router()

updateEventRouter.put(
  '/api/calendars/:calendarId/events/:eventId',
  requireAuth,
  async (req: Request, res: Response) => {
    const { calendarId, eventId } = req.params

    const calendar = await Calendar.findByGroup(calendarId, req.currentUser!)

    if (!calendar) {
      throw new NotFoundError()
    }

    const event = await Event.findByIdAndUpdate(eventId, req.body, {
      new: true,
    })

    res.status(200).send(event)
  }
)

export { updateEventRouter }
