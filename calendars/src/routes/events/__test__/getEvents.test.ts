import request from 'supertest'
import { app } from '../../../app'

const setup = async () => {
  const user = global.signin()

  const { body: createdCalendar } = await request(app)
    .post('/api/calendars')
    .set('Cookie', user)
    .send({ name: 'test cal' })

  return { user, createdCalendar }
}

it('has a route handler for GET /api/calendars/:calendarId/events', async () => {
  const { user, createdCalendar } = await setup()

  await request(app)
    .get(`/api/calendars/${createdCalendar.id}/events`)
    .set('Cookie', user)
    .send()
    .expect(200)

  await request(app)
    .get(`/api/calendars/${createdCalendar.id}/events`)
    .send()
    .expect(401)
})

it('retrieves array of events', async () => {
  const { user, createdCalendar } = await setup()

  await request(app)
    .post(`/api/calendars/${createdCalendar.id}/events`)
    .set('Cookie', user)
    .send({ title: 'one', start: '2020-01-01' })
    .expect(201)

  const { body: getEvents1 } = await request(app)
    .get(`/api/calendars/${createdCalendar.id}/events`)
    .set('Cookie', user)
    .send()
    .expect(200)

  expect(getEvents1.length).toEqual(1)

  await request(app)
    .post(`/api/calendars/${createdCalendar.id}/events`)
    .set('Cookie', user)
    .send({ title: 'two', start: '2020-01-02' })
    .expect(201)

  const { body: getEvents } = await request(app)
    .get(`/api/calendars/${createdCalendar.id}/events`)
    .set('Cookie', user)
    .send()
    .expect(200)

  expect(getEvents.length).toEqual(2)
})
