import express, { Request, Response } from 'express'
import {
  BadRequestError,
  NotFoundError,
  requireAuth,
  validateRequest,
} from '@life.stack/common'
import { body } from 'express-validator'
import { Event } from '../../models/event'
import { Calendar } from '../../models/calendar'

const newEventRouter = express.Router()

newEventRouter.post(
  '/api/calendars/:calendarId/events',
  requireAuth,
  [
    body('title').trim().isString().isLength({ min: 1, max: 128 }),
    body('start').isString(),
    body('end').optional(true).isString(),
    body('location').optional(true).isString(),
    body('allDay').optional(true).isBoolean(),
    body('color').optional(true).isString(),
    body('editable').optional(true).isBoolean(),
    body('notes')
      .optional(true)
      .trim()
      .isString()
      .isLength({ min: 1, max: 1024 }),
  ],
  validateRequest,
  async (req: Request, res: Response) => {
    const { calendarId } = req.params
    const {
      title,
      start,
      end,
      allDay,
      location,
      notes,
      recurring,
      editable,
      color,
    } = req.body

    const calendar = await Calendar.findByGroup(calendarId, req.currentUser!)
    if (!calendar) {
      throw new NotFoundError()
    }

    const event = Event.build({
      calendarId,
      title,
      start,
      end,
      allDay,
      location,
      notes,
      recurring,
      editable,
      color,
    })
    await event.save()

    res.status(201).send(event)
  }
)

export { newEventRouter }
