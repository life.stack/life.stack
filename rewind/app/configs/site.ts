/**
 * EDITME: Site Config and Meta Config
 *
 * Site-wide info and meta data, mostly for information and SEO purpose
 */

import { configDev } from "~/configs";

// For general
export const configSite = {
  domain: configDev.isDevelopment
    ? "rewind.lifestack.test"
    : "production.lifestack.test",

  slug: "life.stack",
  name: "Life.Stack",
  title: "LifeStack: Life Planner",
  description:
    "Life.Stack is a set of tools to track and excel at life. Made by @fubz.",

  links: {
    website: "https://lifestack.test",
    github: "https://github.com/fubz",
    twitter: "https://x.com",
    youtube: "https://youtube.com",
    facebook: "https://facebook.com",
    instagram: "https://instagram.com",
    devTo: "https://dev.to",
    hashnode: "https://hashnode.com",
    showwcase: "https://showwcase.com",
    bitcoin: "btc-address0x0"
  },

  twitter: {
    site: "@the.fubz",
    creator: "@the.fubz",
  },

  navItems: [
    { to: "/", name: "Home", icon: "home" },
    { to: "/calendars", name: "Calendar", icon: "home" },
    { to: "/list", name: "List", icon: "home" },
    { to: "/trackers", name: "Trackers", icon: "home" },
    { to: "/rewards", name: "Rewards", icon: "home" },

    { to: "/about", name: "About", icon: "about" },
    { to: "/components", name: "Components", icon: "components" },
    { to: "/demo", name: "Demo", icon: "demo" },
    { to: "/notes", name: "Notes", icon: "notes" },
  ],
};

// For Remix meta function
export const configMeta = {
  defaultName: configSite?.name,
  defaultTitle: configSite?.title,
  defaultTitleSeparator: "—",
  defaultDescription: configSite?.description,

  locale: "en_US",
  url: configDev.isDevelopment
    ? "http://localhost:5000"
    : `https://${configSite?.domain}`,
  canonicalPath: "/",
  color: "#3399cc", // EDITME
  ogType: "website",
  ogImageAlt: configSite?.title,
  ogImageType: "image/png",
  ogImagePath: "/assets/opengraph/rewinds-og.png",
  twitterImagePath: "/assets/opengraph/rewinds-og.png",
  fbAppId: "",

  author: {
    name: "Michael Ski",
    handle: "@fubz",
    url: "https://michael.ski",
    company: {
      name: "fubz",
      handle: "@fubz",
      url: "https://fubz.dev",
    },
  },

  mailingListName: "The Fubz Mailing List",
};
