import { ActionArgs, json } from '@remix-run/node'

export async function action({request} : ActionArgs) {
  console.log('ping action')
  const json = await request.json()

  console.log(json)
  console.log(request.headers)

  return true
}

export async function loader() {
  return json({
    message: "ping",
    success: true,
  });
}
