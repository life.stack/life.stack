import { json } from "@remix-run/node";
import { useLoaderData } from "@remix-run/react";
import { Debug, Layout } from "~/components";


export async function loader() {

  return json({
    starter: 'test'
  });
}

export default function Route() {
  const loaderData = useLoaderData<typeof loader>();

  return (
    <Layout isSpaced>
      <Debug name="loaderData">{loaderData}</Debug>
    </Layout>
  );
}
