import { json } from "@remix-run/node";
import { useLoaderData } from "@remix-run/react";
import { Debug, Layout } from "~/components";
import { createSitemap } from "~/utils";
import {exec} from 'child_process'
export const handle = createSitemap();

export async function loader() {

  try {
    const {stdout: push} = await  exec('npm run prisma:push')
    const {stdout: seed} = await  exec('npm run prisma:seed')

    return json({
      push,
      seed,
    });

  } catch(error) {
    return json({error})
  }

}

export default function Route() {
  const loaderData = useLoaderData<typeof loader>();

  return (
    <Layout isSpaced>
      <Debug name="loaderData">{loaderData}</Debug>
    </Layout>
  );
}
