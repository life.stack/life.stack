import {
  AnchorText,
  ButtonAnchor,
  ButtonLink,
  LandingImage,
  Layout,
} from "~/components";
import { configSite } from "~/configs";
import { useRootLoaderData } from "~/hooks";
import { CompactDisc, Components, Github, Packages, PeaceHand } from "~/icons";
import { createSitemap } from "~/utils";

export const handle = createSitemap("/", 1);

export default function Route() {
  const { user } = useRootLoaderData();

  return (
    <Layout>
      <section className="mx-auto flex max-w-max flex-wrap items-center justify-center gap-4 py-10 lg:justify-between lg:py-20">
        <div className="max-w-lg space-y-4">
          <div className="prose-config">
            <h1 className="text-xl">
              <span className="block">Life </span>
              <br className="block sm:hidden" />
              <span className="block">Stack</span>
            </h1>
            <p>
              <PeaceHand className="inline-icon link" />
              {user ? <span>Hey {user.name}, </span> : <span>Hey, </span>}
              <span>What is untracked is unmanaged.</span>
              <br className="block sm:hidden" />
              <span>Start managing your life now. Stacking one little habit at a time.</span>
            </p>
          </div>
          <div className="queue">
            <ButtonLink to="/start" size="lg">
              <Components />
              <span>Get started</span>
            </ButtonLink>
            <ButtonAnchor
              to="/about"
              size="lg"
              variant="outline"
            >
              <span>Learn More</span>
            </ButtonAnchor>
          </div>
        </div>

        <aside>
          <LandingImage />
        </aside>
      </section>
    </Layout>
  );
}
