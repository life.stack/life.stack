import {
  Box,
  Button,
  Checkbox,
  Flex,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Input,
  Stack,
  Text,
  useColorModeValue,
} from "@chakra-ui/react";
import { ReactElement } from "react";
import { ActionFunction, Link } from "remix";
import apiFetch from "~/utils/apiFetch";

type AuthFormProps = {
  signup?: Boolean;
  children: ReactElement;
  errors?: object;
};

export const action: ActionFunction = async ({ request }) => {
  const form = await request.formData();
  const email = await form.get("email");
  const password = await form.get("password");
  console.log(form);
  const data = await apiFetch({
    uri: "/api/users/signin",
    body: { email, password },
    redirectTo: "/",
  });
  console.log(data);
  return data;
};

export default function AuthForm({
  signup,
  children,
  errors,
}: AuthFormProps): ReactElement {
  return (
    <>
      <form method="post">
        <Flex
          align="center"
          justify="center"
          bg={useColorModeValue("gray.50", "gray.800")}
        >
          <Stack spacing={8} mx="auto" maxW="lg" px={6}>
            {children}
            <Box
              rounded="lg"
              bg={useColorModeValue("white", "gray.700")}
              boxShadow="lg"
              p={8}
            >
              <Stack spacing={4}>
                {signup && (
                  <FormControl id="displayName">
                    <FormLabel>Name</FormLabel>
                    <Input
                      name="dispalyName"
                      placeholder="What should we call you?"
                    />
                    <FormErrorMessage>
                      {/*{formState.errors.displayName}*/}
                    </FormErrorMessage>
                  </FormControl>
                )}
                <FormControl id="email">
                  <FormLabel>Email address</FormLabel>
                  <Input type="email" name="email" />
                  {/*<FormErrorMessage>{formState.errors.email}</FormErrorMessage>*/}
                </FormControl>
                <FormControl id="password">
                  <FormLabel>Password</FormLabel>
                  <Input type="password" name="password" />
                  <FormErrorMessage>
                    {/*{formState.errors.password}*/}
                  </FormErrorMessage>
                </FormControl>
                <Stack spacing={10}>
                  <Stack
                    direction={{ base: "column", sm: "row" }}
                    align="start"
                    justify="space-between"
                  >
                    <Checkbox>Remember me</Checkbox>
                    <Link to="/forgotPassword" color="blue.400">
                      Forgot password?
                    </Link>
                  </Stack>
                  <Button
                    type="submit"
                    bg="blue.400"
                    color="white"
                    _hover={{
                      bg: "blue.500",
                    }}
                  >
                    {signup ? "Sign up" : "Sign in"}
                  </Button>
                  {!signup && (
                    <Text align="center" fontSize="sm" color="gray.600">
                      <Link color="blue.400" to="/signup">
                        or sign up for an account now!
                      </Link>
                    </Text>
                  )}
                </Stack>
              </Stack>
            </Box>
          </Stack>
        </Flex>
      </form>
    </>
  );
}
