import { Outlet } from "remix";

export default function ListsRoute() {
  return (
    <div>
      <h1>Lists🤪</h1>
      <main>
        <Outlet />
      </main>
    </div>
  );
}
