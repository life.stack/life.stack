import { redirect } from "remix";

type apiFetchProps = {
  redirectTo?: string;
  body: object;
  uri: string;
};

const hostname = "https://ticketing.dev";

export default async function apiFetch({
  uri,
  body,
  redirectTo,
}: apiFetchProps) {
  const response = await fetch(new URL(uri, hostname).href, {
    method: "POST",
    body: JSON.stringify(body),
    headers: { "Content-Type": "application/json" },
  });

  const data = await response.json();
  // console.log('data', data)

  if (response.ok) {
    console.log("response, ok", data);
    if (redirectTo) {
      return redirect(redirectTo);
    }
    return data;
  } else {
    return data;
  }
}
