import { Box, BoxProps, Flex, Icon, useColorModeValue } from '@chakra-ui/react'
// import Link from 'next/link'
import { ReactNode } from 'react'
// import Link from 'remix'

type NavItemProps = BoxProps & {
  icon?: any
  children?: ReactNode
  href?: object | string
  rest?: object
}

const NavItem = ({ icon, children, href, ...rest }: NavItemProps) => {
  return (
    // @ts-ignore
    <Box as={href ? 'a' : 'div'} href={href ? href : undefined}>
      <Flex
        align="center"
        px="4"
        pl="4"
        py="3"
        cursor="pointer"
        color={useColorModeValue('inherit', 'gray.400')}
        _hover={{
          bg: useColorModeValue('gray.100', 'gray.900'),
          color: useColorModeValue('gray.900', 'gray.200'),
        }}
        role="group"
        fontWeight="semibold"
        transition=".15s ease"
        {...rest}
      >
        {icon && (
          <Icon
            mr="2"
            boxSize="4"
            _groupHover={{
              // eslint-disable-next-line react-hooks/rules-of-hooks
              color: useColorModeValue('gray.600', 'gray.300'),
            }}
            as={icon}
          />
        )}
        {children}
      </Flex>
    </Box>
  )
}

export default NavItem
