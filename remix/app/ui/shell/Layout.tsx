import {
  Box,
  Drawer,
  DrawerContent,
  DrawerOverlay,
  Flex,
  IconButton,
  Input,
  InputGroup,
  InputLeftElement,
  useColorModeValue,
  useDisclosure,
} from "@chakra-ui/react";
import { FiMenu, FiSearch } from "react-icons/fi";
import SidebarContent from "./SidebarContent";
import UserNav from "./UserNav";
import GroupNav from "./GroupNav";
import Notifications from "./Notifications";
// import { useGroups } from "../../api/useGroups";
import SignupNav from "./SignupNav";
// import LoadingCircle from "../util/LoadingCircle";
import { useEffect } from "react";

function Layout({ children }) {
  const sidebar = useDisclosure();
  // const router = useRouter();
  // const { user, hasUser, isLoading } = useUsers();
  // const { groups, hasGroup } = useGroups();


  // useEffect(() => {
  //   if (
  //     !hasGroup &&
  //     !(
  //       router.pathname === "/" ||
  //       router.pathname.startsWith("/onboard") ||
  //       router.pathname.startsWith("/auth")
  //     )
  //   ) {
  //     router.push("/onboard");
  //   }
  // }, [hasGroup, hasUser, router]);

  return (
    <Box
      as="section"
      bg={useColorModeValue("gray.50", "gray.700")}
      minH="100vh"
    >
      <SidebarContent
        display={{ base: "none", md: "unset" }}
        // currentUser={user}
        // groups={groups}
        // hasGroup={hasGroup}
      />
      <Drawer
        isOpen={sidebar.isOpen}
        onClose={sidebar.onClose}
        placement="left"
      >
        <DrawerOverlay />
        <DrawerContent>
          <SidebarContent
            w="full"
            borderRight="none"
            // currentUser={user}
            // groups={groups}
            // hasGroup={hasGroup}
          />
        </DrawerContent>
      </Drawer>
      <Box ml={{ base: 0, md: 60 }} transition=".3s ease">
        <Flex
          as="header"
          align="center"
          justify="space-between"
          w="full"
          px="4"
          bg={useColorModeValue("white", "gray.800")}
          borderBottomWidth="1px"
          borderColor={useColorModeValue("inherit", "gray.700")}
          h="14"
        >
          <IconButton
            aria-label="Menu"
            display={{ base: "inline-flex", md: "none" }}
            onClick={sidebar.onOpen}
            icon={<FiMenu />}
            size="sm"
          />
          <InputGroup w="96" display={{ base: "none", md: "flex" }}>
            <InputLeftElement color="gray.500">
              <FiSearch />
            </InputLeftElement>
            <Input placeholder="Search for life..." />
          </InputGroup>

          <Flex align="center">
            {/*{!user ? <SignupNav /> : <GroupNav />}*/}
            <Notifications />
            {/*<UserNav currentUser={user} />*/}
          </Flex>
        </Flex>

        <Box as="main" px={[0, 4]} py={[1, 4]}>
          {children}
          {/*{isLoading ? <LoadingCircle /> : children}*/}
        </Box>
      </Box>
    </Box>
  );
}

export default Layout;
