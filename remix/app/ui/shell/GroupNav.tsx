import { Button, ButtonGroup, Select, Stack } from '@chakra-ui/react'
import { useRouter } from 'next/router'
import { useGroups } from '../../api/useGroups'
import GroupIcon from '../groups/GroupIcon'

const GroupsNav = () => {
  const { groups } = useGroups()
  // console.log('Default Group: ', JSON.stringify(groups[0]))

  if (groups && groups.length > 0) {
    return (
      <Stack spacing={3}>
        <Select placeholder={`Group: ${groups[0].name}`} icon={<GroupIcon />}>
          {groups.map((group) => {
            return (
              <option key={group.id} value={group.id}>
                {group.name}
              </option>
            )
          })}
        </Select>
      </Stack>
    )
  } else {
    return <NoGroups />
  }
}

const NoGroups = () => {
  const router = useRouter()
  return (
    <ButtonGroup variant="outline" size="sm">
      <Button colorScheme="blue" onClick={() => router.push('/onboard/join')}>
        Join a Group
      </Button>
      <Button
        colorScheme="purple"
        onClick={() => router.push('/onboard/create')}
      >
        Create a Group
      </Button>
    </ButtonGroup>
  )
}

export default GroupsNav
