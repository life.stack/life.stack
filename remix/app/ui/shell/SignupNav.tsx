import { Button, ButtonGroup } from '@chakra-ui/react'
import { useRouter } from 'next/router'

const SignupNav = () => {
  const router = useRouter()

  return (
    <ButtonGroup variant="solid" size="sm">
      <Button colorScheme="green" onClick={() => router.push('/auth/signup')}>
        Sign Up
      </Button>
      <Button colorScheme="blue" onClick={() => router.push('/auth/signin')}>
        Sign In
      </Button>
    </ButtonGroup>
  )
}

export default SignupNav
