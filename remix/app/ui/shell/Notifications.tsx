import { Icon, IconButton } from '@chakra-ui/react'
import { FaBell } from 'react-icons/fa'

const Notifications = () => {
  return (
    <IconButton
      mx={2}
      rounded="full"
      aria-label="Notifications"
      icon={<Icon color="gray.500" as={FaBell} cursor="pointer" />}
    />
  )
}

export default Notifications
