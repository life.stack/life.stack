import {
  GroupInvitationExpiredEvent,
  Publisher,
  Subjects,
} from '@life.stack/common'

export class GroupInvitationExpiredPublisher extends Publisher<GroupInvitationExpiredEvent> {
  subject: Subjects.GroupInvitationExpired = Subjects.GroupInvitationExpired

  publish(data: GroupInvitationExpiredEvent['data']): Promise<void> {
    return super.publish(data)
  }
}
