import { OrderCreatedListener } from '../orderCreatedListener'
import { natsWrapper } from '../../../natsWrapper'
import { OrderCreatedEvent, OrderStatus } from '@life.stack/common'
import { expirationQueue } from '../../../queues/expirationQueue'

const setup = async () => {
  // Create an instance of the listener
  const listener = new OrderCreatedListener(natsWrapper.client)
  // Create a fake data event
  const data: OrderCreatedEvent['data'] = {
    version: 0,
    id: 'mongoose.Types.ObjectId().toHexString()',
    userId: 'mongoose.Types.ObjectId().toHexString().userId',
    status: OrderStatus.Created,
    expiresAt: new Date().toISOString(),
    ticket: {
      id: 'a-ticket-id',
      price: 42.56,
    },
  }

  // create a fake message object
  // @ts-ignore
  const msg: Message = {
    ack: jest.fn(),
  }

  return { listener, data, msg }
}

it('adds a message to the expiration queue', async () => {
  const { listener, data, msg } = await setup()

  await listener.onMessage(data, msg)

  expect(expirationQueue.add).toHaveBeenCalled()
})

it('acks the message', async () => {
  const { listener, data, msg } = await setup()

  await listener.onMessage(data, msg)

  expect(msg.ack).toHaveBeenCalled()
})
