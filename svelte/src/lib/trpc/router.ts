import { lists } from "$lib/trpc/routes/lists"
import { t } from "$lib/trpc/t"
import type { inferRouterInputs, inferRouterOutputs } from "@trpc/server"

export const router = t.router({
  lists,
})

export type Router = typeof router

// 👇 type helpers 💡
export type RouterInputs = inferRouterInputs<Router>
export type RouterOutputs = inferRouterOutputs<Router>
