import type { RequestEvent } from "@sveltejs/kit"
import type { inferAsyncReturnType } from "@trpc/server"

export async function createContext(event: RequestEvent) {
  try {
    const sessionId = event.cookies.get("express:ss")

    return { sessionId }
  } catch {
    return { sessionId: "" }
  }
}

export type Context = inferAsyncReturnType<typeof createContext>
