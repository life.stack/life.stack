import { auth } from "$lib/trpc/middleware/auth"
import { logger } from "$lib/trpc/middleware/logger"
import { t } from "$lib/trpc/t"
import { z } from "zod"

export const lists = t.router({
  signin: t.procedure
    .use(logger)
    .input(
      z.object({
        email: z.string().email(),
        password: z.string(),
      })
    )
    .mutation(({ input }) => [{ id: 0 }]),
})
