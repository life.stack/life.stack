import { auth } from "$lib/trpc/middleware/auth"
import { logger } from "$lib/trpc/middleware/logger"
import { t } from "$lib/trpc/t"
import { z } from "zod"

export const lists = t.router({
  list: t.procedure
    .use(logger)
    .input(z.string().optional())
    .query(({ input }) => [{ id: 0 }]),
})
