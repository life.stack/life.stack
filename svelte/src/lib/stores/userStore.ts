import { writable } from "svelte/store"

export const userStore = writable(null)

export async function signin(email: string, password: string) {
  try {
    const response = await fetch("https://lifestack.test/api/auth/signin", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ email, password }),
    })
    const userData = await response.json()
    console.log({ userData })
    userStore.set(userData)
  } catch (error) {
    console.error("Login failed:", error)
    // Handle errors appropriately
  }
}
