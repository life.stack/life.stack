import type { RequestEvent } from "@sveltejs/kit"
import { signin } from "$lib/stores/userStore.js"

export const actions = {
  signin: async (event: RequestEvent) => {
    const data = await event.request.formData()
    const email = data.get("email")?.toString()
    const password = data.get("password")?.toString()

    try {
      if (!email || !password) {
        throw new Error("empty email or password")
      }
      await signin(email, password)
    } catch (error) {
      console.error(error)
    }
  },
}
