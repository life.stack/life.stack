import jwt from 'jsonwebtoken'
import { UserDoc } from '../models/user'

const generateJWT = (existingUser: UserDoc): string => {
  // Generate JWT
  const userJwt = jwt.sign(
    {
      id: existingUser.id,
      email: existingUser.email,
      groups: existingUser.groups,
    },
    process.env.JWT_KEY!
  )

  return userJwt
}

export default generateJWT
