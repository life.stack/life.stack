import { Request, Response, NextFunction } from 'express'
import { NotAuthorizedError } from '@life.stack/common'

export const requireAuthButNotGroup = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (!req.currentUser) {
    throw new NotAuthorizedError()
  }

  next()
}
