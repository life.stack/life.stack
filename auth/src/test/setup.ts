import { MongoMemoryServer } from 'mongodb-memory-server'
import mongoose from 'mongoose'
import jwt from 'jsonwebtoken'
import { app } from '../app'
import request from 'supertest'

declare global {
  function signin(userId?: string): string[]
  function signup(userId?: mongoose.Types.ObjectId): Promise<string[]>
}

jest.mock('../natsWrapper')
jest.setTimeout(60000)

let mongo: MongoMemoryServer

beforeAll(async () => {
  // Our app needs JWT_KEY defined
  process.env.JWT_KEY = 'jesttest'

  mongo = await MongoMemoryServer.create()
  const mongoUri = mongo.getUri()

  await mongoose.connect(mongoUri, {})
})

beforeEach(async () => {
  jest.clearAllMocks()
  const collections = await mongoose.connection.db.collections()

  for (let collection of collections) {
    await collection.deleteMany({})
  }
})

afterAll(async () => {
  await mongo.stop()
  await mongoose.connection.close()
})

global.signup = async (userId = new mongoose.Types.ObjectId()) => {
  const email = `${userId}@test.com`
  const password = 'areallongpassword'
  const displayName = `Tester-${userId}`

  const response = await request(app)
    .post('/api/users/signup')
    .send({
      displayName,
      email,
      password,
    })
    .expect(201)

  const cookie = response.get('Set-Cookie')

  return cookie
}

global.signin = (userId?: string) => {
  // Build a JWT payload. { id, email }
  const id = userId || new mongoose.Types.ObjectId()
  const payload = {
    id,
    email: `${id}@test.com`,
    groups: [new mongoose.Types.ObjectId()],
  }

  // create the jwt
  const token = jwt.sign(payload, process.env.JWT_KEY!)

  // build the session object. { jwt: MY_JWT }
  const session = { jwt: token }

  // Turn that session into JSON
  const sessionString = JSON.stringify(session)

  // Take JSON and encode it as base64
  const base64 = Buffer.from(sessionString).toString('base64')

  // return a string that's the cookie with encoded data
  return [`express:sess=${base64}`]
}
