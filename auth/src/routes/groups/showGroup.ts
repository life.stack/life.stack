import express from 'express'
import {
  currentUser,
  NotFoundError,
  requireAuth,
  validateParams,
} from '@life.stack/common'
import { Group } from '../../models/group'
import { GroupSchema } from './GroupSchema'

const showGroupRouter = express.Router()

showGroupRouter.get(
  '/api/groups/:id',
  currentUser,
  requireAuth,
  validateParams(GroupSchema.pick({ id: true }), async (req, res) => {
    const { id } = req.params
    const group = await Group.findById(id) //.populate(['users', 'owners'])
    if (!group) {
      throw NotFoundError
    }

    res.send(group)
  })
)

export { showGroupRouter }
