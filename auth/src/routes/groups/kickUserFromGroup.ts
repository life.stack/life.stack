import express from 'express'
import {
  BadRequestError,
  currentUser,
  NotAuthorizedError,
  NotFoundError,
  requireAuth,
  validate,
} from '@life.stack/common'
import { Group } from '../../models/group'
import { User } from '../../models/user'
import { z } from 'zod'
import some from 'lodash/some'

const kickUserFromGroupRouter = express.Router()

kickUserFromGroupRouter.post(
  '/api/groups/:groupId/kick',
  currentUser,
  requireAuth,
  validate(
    {
      body: z.object({ userId: z.string() }),
      params: z.object({ groupId: z.string() }),
    },
    async (req, res) => {
      const { userId } = req.body
      const { groupId } = req.params

      const group = await Group.findOne({
        id: groupId,
        owners: req.currentUser!.id,
      })
      let kickedUser = await User.findById(userId)

      if (!group || !kickedUser) {
        throw new NotFoundError()
      }

      if (
        !some(group.owners, (owner) => owner.toString() === req.currentUser!.id)
      ) {
        throw new NotAuthorizedError()
      }

      if (!group.users.includes(userId)) {
        throw new BadRequestError('User does not exist in this group')
      }

      group.users = group.users.filter((u) => u.toString() !== userId)
      kickedUser.groups = kickedUser.groups.filter(
        (g) => g.toString() !== groupId
      )

      await group.save()
      await kickedUser.save()

      res.status(200).send(group)
    }
  )
)

export { kickUserFromGroupRouter }
