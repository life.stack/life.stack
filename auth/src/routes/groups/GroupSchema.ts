import { z } from 'zod'
import { UserProfileSchema } from '@life.stack/common/build/schemas'

export const GroupSchema = z.object({
  id: z.string(),
  version: z.number(),
  name: z.string().min(1).max(128).optional(),
  owners: z.array(UserProfileSchema),
  users: z.array(UserProfileSchema),
})

export interface Group extends z.infer<typeof GroupSchema> {}
