import { app } from '../../../app'
import request from 'supertest'
import { UserDoc } from '../../../models/user'

const createUser = async (): Promise<UserDoc> => {
  const { body: signupUser } = await request(app)
    .post('/api/users/signup')
    .send({
      displayName: 'Testy',
      email: 'grouptest@test.com',
      password: 'passwordhelloworld',
    })

  return signupUser
}

it('has a route handler listening to /api/groups/currentgroups for GET requests', async () => {
  const response = await request(app).get('/api/groups/currentgroups').send()

  expect(response.status).not.toEqual(404)
})

it('can only be access if user is signed in', async () => {
  await request(app).get('/api/groups/currentgroups').send().expect(401)
})

it('current groups shows details for all groups assigned to user', async () => {
  const signupUser = await createUser()
  const cookie = global.signin(signupUser.id)

  const { body: group } = await request(app)
    .post('/api/groups')
    .set('Cookie', cookie)
    .send({
      name: 'Our Family',
    })
    .expect(201)

  expect(group).toBeDefined()

  const { body: currentGroups } = await request(app)
    .get('/api/groups/currentgroups')
    .set('Cookie', cookie)

  expect(currentGroups[0].id).toEqual(group.id)
  expect(currentGroups[0].name).toEqual(group.name)
})
