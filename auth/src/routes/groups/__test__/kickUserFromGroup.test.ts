import { app } from '../../../app'
import request from 'supertest'
import { UserDoc } from '../../../models/user'
import mongoose from 'mongoose'
import { Invite } from '../../../models/invite'
import { natsWrapper } from '../../../natsWrapper'
import { Group } from '../../../models/group'

const createUser = async (): Promise<UserDoc> => {
  const { body: signupUser } = await request(app)
    .post('/api/users/signup')
    .send({
      displayName: 'Testy',
      email: 'grouptest@test.com',
      password: 'passwordhelloworld',
    })

  return signupUser
}

it('has a route handler listening to /api/groups/:groupId/kick for POST requests', async () => {
  const response = await request(app)
    .post(`/api/groups/${new mongoose.Types.ObjectId().toHexString()}/kick`)
    .send()

  expect(response.status).not.toEqual(404)
})

it('can only be access if user is signed in', async () => {
  await request(app)
    .post(`/api/groups/${new mongoose.Types.ObjectId().toHexString()}/kick`)
    .send()
    .expect(401)
})

it('Kicks a user from the group', async () => {
  let user1 = await global.signup()

  const groupResponse = await request(app)
    .post('/api/groups')
    .set('Cookie', user1)
    .send({
      name: 'Our Family',
    })
    .expect(201)

  const group = groupResponse.body
  user1 = groupResponse.get('Set-Cookie')

  const { body: invite } = await request(app)
    .post(`/api/groups/${group.id}/invite`)
    .set('Cookie', user1)
    .send({})
    .expect(201)

  const secondUserResponse = await request(app).post('/api/users/signup').send({
    displayName: 'Kicked',
    email: 'Kicked@test.com',
    password: 'passwordhelloworld',
  })

  const secondUser = secondUserResponse.body
  const user2 = secondUserResponse.get('Set-Cookie')

  await request(app)
    .post('/api/groups/accept')
    .set('Cookie', user2)
    .send({
      inviteKey: invite.inviteKey,
    })
    .expect(201)

  let groupInDb = await Group.findById(group.id)
  expect(groupInDb).toBeDefined()
  expect(groupInDb!.users.includes(secondUser.id)).toEqual(true)

  // console.log({ user1, user2, secondUser })

  await request(app)
    .post(`/api/groups/${group.id}/kick`)
    .set('Cookie', user1)
    .send({ userId: secondUser.id })
    .expect(200)

  groupInDb = await Group.findById(group.id)
  expect(groupInDb!.users.includes(secondUser.id)).toEqual(false)
})

it('Only an owner can kick from the group', async () => {
  const signupUser = await createUser()

  const { body: group } = await request(app)
    .post('/api/groups')
    .set('Cookie', global.signin(signupUser.id))
    .send({
      name: 'Our Family',
    })
    .expect(201)

  const { body: invite } = await request(app)
    .post(`/api/groups/${group.id}/invite`)
    .set('Cookie', global.signin(signupUser.id))
    .send({})
    .expect(201)

  const { body: secondUser } = await request(app)
    .post('/api/users/signup')
    .send({
      displayName: 'Kicked',
      email: 'Kicked@test.com',
      password: 'passwordhelloworld',
    })

  await request(app)
    .post('/api/groups/accept')
    .set('Cookie', global.signin(secondUser.id))
    .send({
      inviteKey: invite.inviteKey,
    })
    .expect(201)

  await request(app)
    .post(`/api/groups/${group.id}/kick`)
    .set('Cookie', global.signin(secondUser.id))
    .send({ userId: signupUser.id })
    .expect(404)

  const groupInDb = await Group.findById(group.id).lean()
  expect(groupInDb!.owners.includes(secondUser.id)).toEqual(false)
})
