import { app } from '../../../app'
import request from 'supertest'
import { Group } from '../../../models/group'
import { User, UserDoc } from '../../../models/user'

const createUser = async (): Promise<UserDoc> => {
  const { body: signupUser } = await request(app)
    .post('/api/users/signup')
    .send({
      displayName: 'Testy',
      email: 'grouptest@test.com',
      password: 'passwordhelloworld',
    })

  return signupUser
}

it('has a route handler listening to /api/groups for POST requests', async () => {
  const response = await request(app).post('/api/groups').send({})

  expect(response.status).not.toEqual(404)
})

it('can only be access if user is signed in', async () => {
  await request(app).post('/api/groups').send({}).expect(401)
})

it('a group requires a name', async () => {
  await request(app)
    .post('/api/groups')
    .set('Cookie', global.signin())
    .send({
      name: '',
    })
    .expect(400)
})

it('creating a group adds the doc to the database and the user owns it', async () => {
  const signupUser = await createUser()

  const response = await request(app)
    .post('/api/groups')
    .set('Cookie', global.signin(signupUser.id))
    .send({
      name: 'Our Family',
    })
    .expect(201)

  expect(response.body.id).toBeDefined()
  expect(response.body.name).toEqual('Our Family')
  expect(response.body.owners[0].toString()).toEqual(signupUser.id)

  const group = await Group.findById(response.body.id)
  expect(group).toBeDefined()
})

it('creating a group adds the group id to the user', async () => {
  const signupUser = await createUser()

  const cookie = global.signin(signupUser.id)
  const response = await request(app)
    .post('/api/groups')
    .set('Cookie', cookie)
    .send({
      name: 'Our Family',
    })
    .expect(201)

  const user = await User.findById(signupUser.id)
  expect(user?.groups[0].toString()).toEqual(response.body.id)
})
