import { app } from '../../../app'
import request from 'supertest'
import { UserDoc } from '../../../models/user'
import mongoose from 'mongoose'
import { Invite } from '../../../models/invite'
import { natsWrapper } from '../../../natsWrapper'

const createUser = async (): Promise<UserDoc> => {
  const { body: signupUser } = await request(app)
    .post('/api/users/signup')
    .send({
      displayName: 'Testy',
      email: 'grouptest@test.com',
      password: 'passwordhelloworld',
    })

  return signupUser
}

it('has a route handler listening to /api/groups/:id/invite for POST requests', async () => {
  const response = await request(app)
    .post(`/api/groups/${new mongoose.Types.ObjectId().toHexString()}/invite`)
    .send()

  expect(response.status).not.toEqual(404)
})

it('can only be access if user is signed in', async () => {
  await request(app)
    .post(`/api/groups/${new mongoose.Types.ObjectId().toHexString()}/invite`)
    .send()
    .expect(401)
})

it('Creates an invite code for the specified group', async () => {
  const signupUser = await createUser()

  const { body: group } = await request(app)
    .post('/api/groups')
    .set('Cookie', global.signin(signupUser.id))
    .send({
      name: 'Our Family',
    })
    .expect(201)

  const { body: invite } = await request(app)
    .post(`/api/groups/${group.id}/invite`)
    .set('Cookie', global.signin(signupUser.id))
    .send({})
    .expect(201)

  expect(invite.inviteKey).toBeDefined()
  expect(invite.inviteKey).toHaveLength(10)

  const inviteDoc = await Invite.findById(invite.id)
  expect(inviteDoc).toBeDefined()
})

it('A user cannot create an invite for a group they do not own', async () => {
  const signupUser = await createUser()

  const { body: group } = await request(app)
    .post('/api/groups')
    .set('Cookie', global.signin(signupUser.id))
    .send({
      name: 'Our Family',
    })
    .expect(201)

  const { body: invite } = await request(app)
    .post(`/api/groups/${group.id}/invite`)
    .set('Cookie', global.signin())
    .send({})
    .expect(400)
})

it('emits a GroupInvitationCreated event', async () => {
  const signupUser = await createUser()

  const { body: group } = await request(app)
    .post('/api/groups')
    .set('Cookie', global.signin(signupUser.id))
    .send({
      name: 'Our Family',
    })
    .expect(201)

  const { body: invite } = await request(app)
    .post(`/api/groups/${group.id}/invite`)
    .set('Cookie', global.signin(signupUser.id))
    .send({})
    .expect(201)

  expect(natsWrapper.client.publish).toHaveBeenCalled()
})
