import express, { Request, Response } from 'express'
import { body } from 'express-validator'
import { currentUser, NotFoundError, validateRequest } from '@life.stack/common'
import { Group } from '../../models/group'
import { User } from '../../models/user'
import { requireAuthButNotGroup } from '../../middleware/requireAuthButNotGroup'
import generateJWT from '../../services/generateJWT'

const router = express.Router()

router.post(
  '/api/groups',
  currentUser,
  requireAuthButNotGroup,
  [body('name').trim().isLength({ min: 1 }).withMessage('Name is required')],
  validateRequest,
  async (req: Request, res: Response) => {
    const { name } = req.body

    const user = await User.findById(req.currentUser?.id)
    if (!user) {
      throw NotFoundError
    }

    const group = Group.build({ name, owner: req.currentUser!.id })
    await group.save()

    user.groups.push(group.id)
    await user.save()

    // Generate JWT
    const userJwt = generateJWT(user)

    // Store JWT on the session
    req.session = { jwt: userJwt }

    res.status(201).send(group)
  }
)

export { router as createGroupRouter }
