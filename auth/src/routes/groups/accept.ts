import express, { Request, Response } from 'express'
import {
  BadRequestError,
  currentUser,
  validateRequest,
} from '@life.stack/common'
import { body } from 'express-validator'
import { Invite } from '../../models/invite'
import { Group } from '../../models/group'
import generateJWT from '../../services/generateJWT'
import { User } from '../../models/user'
import { requireAuthButNotGroup } from '../../middleware/requireAuthButNotGroup'

const router = express.Router()

router.post(
  '/api/groups/accept',
  currentUser,
  requireAuthButNotGroup,
  [
    body('inviteKey')
      .trim()
      .isLength({ min: 10, max: 10 })
      .withMessage('Invite Code is not valid'),
  ],
  validateRequest,
  async (req: Request, res: Response) => {
    const { inviteKey } = req.body

    const invite = await Invite.findOne({ inviteKey })

    if (!invite) {
      throw new BadRequestError('Invite Code is not valid')
    }

    const group = await Group.findById(invite.group)
    let user = await User.findById(req.currentUser!.id)
    if (!group || !user) {
      throw new BadRequestError('Invite Code is not valid')
    }
    if (group.users.includes(user.id)) {
      throw new BadRequestError('User has already joined group')
    }

    await invite.delete()

    group.users.push(req.currentUser!.id)
    await group.save()

    user.groups.push(group.id)
    user = await user.save()

    // Generate JWT
    const userJwt = generateJWT(user)

    // Store JWT on the session
    req.session = { jwt: userJwt }

    res.status(201).send(group)
  }
)

export { router as acceptGroupRouter }
