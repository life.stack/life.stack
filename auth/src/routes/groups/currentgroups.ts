import express, { Request, Response } from 'express'
import { currentUser, NotFoundError } from '@life.stack/common'
import { User } from '../../models/user'
import { requireAuthButNotGroup } from '../../middleware/requireAuthButNotGroup'

const router = express.Router()

router.get(
  '/api/groups/currentgroups',
  currentUser,
  requireAuthButNotGroup,
  async (req: Request, res: Response) => {
    const user = await User.findById(req.currentUser!.id).populate('groups')
    if (!user) {
      throw NotFoundError
    }

    res.send(user.groups)
  }
)

export { router as currentgroupsRouter }
