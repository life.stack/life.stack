import express from 'express'
import {
  BadRequestError,
  currentUser,
  NotAuthorizedError,
  NotFoundError,
  requireAuth,
  validate,
} from '@life.stack/common'
import { Group } from '../../models/group'
import { z } from 'zod'
import some from 'lodash/some'

const addOwnerToGroup = express.Router()

addOwnerToGroup.post(
  '/api/groups/:groupId/owner',
  currentUser,
  requireAuth,
  validate(
    {
      body: z.object({ userId: z.string() }),
      params: z.object({ groupId: z.string() }),
    },
    async (req, res) => {
      const { userId } = req.body
      const { groupId } = req.params

      const group = await Group.findById(groupId)

      if (!group) {
        throw new NotFoundError()
      }

      if (
        !some(group.owners, (owner) => owner.toString() === req.currentUser!.id)
      ) {
        throw new NotAuthorizedError()
      }

      if (!group.users.includes(userId)) {
        throw new BadRequestError('User does not exist in this group')
      }

      if (group.owners.includes(userId)) {
        throw new BadRequestError('User is already an owner')
      }

      group.owners.push(userId)

      await group.save()

      res.status(200).send(group)
    }
  )
)

export { addOwnerToGroup }
