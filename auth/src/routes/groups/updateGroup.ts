import express from 'express'
import {
  currentUser,
  NotFoundError,
  requireAuth,
  validate,
} from '@life.stack/common'
import { Group } from '../../models/group'
import { GroupSchema } from './GroupSchema'

const updateGroupRouter = express.Router()

updateGroupRouter.post(
  '/api/groups/:id/update',
  currentUser,
  requireAuth,
  validate(
    {
      params: GroupSchema.pick({ id: true }),
      body: GroupSchema.pick({ name: true }),
    },
    async (req, res) => {
      const { id } = req.params
      const { name } = req.body

      const group = await Group.findById(id) //.populate(['users', 'owners'])

      if (!group || !group.owners.includes(req.currentUser!.id)) {
        throw NotFoundError
      }

      group.set({ name })
      await group.save()

      res.send(group)
    }
  )
)

export { updateGroupRouter }
