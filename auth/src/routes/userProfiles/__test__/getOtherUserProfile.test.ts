import { app } from '../../../app'
import request from 'supertest'

const createUser = async () => {
  const response = await request(app).post('/api/users/signup').send({
    displayName: 'One',
    email: 'One@test.com',
    password: 'OneLongPassword123456',
  })

  return response.body
}

it('has a route handler listening to /api/users/profile/:otherUserId for GET requests', async () => {
  const user = await createUser()
  const response = await request(app)
    .get(`/api/users/profile/${user.id}`)
    .set('Cookie', global.signin())
    .send()

  expect(response.status).not.toEqual(404)
})

it('can only be access if user is signed in', async () => {
  await request(app).get('/api/users/profile/otherUserId').send().expect(401)
})

it('user one can get user twos profile', async () => {
  const response1 = await request(app)
    .post('/api/users/signup')
    .send({
      displayName: 'One',
      email: 'One@test.com',
      password: 'OneLongPassword123456',
    })
    .expect(201)

  let user1 = response1.get('Set-Cookie')

  const createGroupResponse = await request(app)
    .post('/api/groups')
    .set('Cookie', user1)
    .send({
      name: 'Our Family',
    })
    .expect(201)

  user1 = createGroupResponse.get('Set-Cookie')

  const response2 = await request(app)
    .post('/api/users/signup')
    .send({
      displayName: 'Two',
      email: 'Two@test.com',
      password: 'OneLongPassword123456',
    })
    .expect(201)

  const user2 = response2.get('Set-Cookie')
  const user2Id = response2.body.id
  const user2City = 'Private Town'

  await request(app)
    .post('/api/users/profile')
    .set('Cookie', user2)
    .send({ address: { city: user2City } })
    .expect(200)

  const { body: notGroupedProfile } = await request(app)
    .get(`/api/users/profile/${user2Id}`)
    .set('Cookie', user1)
    .send()
    .expect(200)

  expect(notGroupedProfile.id).toEqual(user2Id)
  expect(notGroupedProfile.address).toBeUndefined()
})
