import { app } from '../../../app'
import request from 'supertest'

it('has a route handler listening to /api/users/profiles for POST requests', async () => {
  const response = await request(app).post('/api/users/profile').send({})

  expect(response.status).not.toEqual(404)
})

it('can only be access if user is signed in', async () => {
  await request(app).post('/api/users/profile').send({}).expect(401)
})

it('creating a user profile starts with a first name', async () => {
  await request(app)
    .post('/api/users/profile')
    .set('Cookie', global.signin())
    .send({
      firstName: '',
    })
    .expect(400)

  await request(app)
    .post('/api/users/profile')
    .set('Cookie', global.signin())
    .send({
      firstName: 'Tester',
    })
    .expect(200)
})

it('user profile can be updated', async () => {
  const user = global.signin()

  const { body: userProfile } = await request(app)
    .post('/api/users/profile')
    .set('Cookie', user)
    .send({
      firstName: 'Tester',
    })
    .expect(200)

  const { body: updatedProfile } = await request(app)
    .post('/api/users/profile')
    .set('Cookie', user)
    .send({
      lastName: 'Lastname',
    })
    .expect(200)

  expect(userProfile.firstName).toEqual(updatedProfile.firstName)
  expect(updatedProfile.firstName).toEqual('Tester')
  expect(updatedProfile.lastName).toEqual('Lastname')
})
