import express, { Request, Response } from 'express'
import { currentUser } from '@life.stack/common'
import { UserProfile } from '../../models/userProfile'
import { requireAuthButNotGroup } from '../../middleware/requireAuthButNotGroup'

const getCurrentUserProfileRouter = express.Router()

getCurrentUserProfileRouter.get(
  '/api/users/profile',
  currentUser,
  requireAuthButNotGroup,
  async (req: Request, res: Response) => {
    const userProfile = await UserProfile.findById(req.currentUser!.id)

    res.status(200).send(userProfile)
  }
)

export { getCurrentUserProfileRouter }
