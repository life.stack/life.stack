import express, { Request, Response } from 'express'
import {
  BadRequestError,
  currentUser,
  NotFoundError,
  requireAuth,
  validateParams,
} from '@life.stack/common'
import { UserProfile } from '../../models/userProfile'
import { Group } from '../../models/group'
import { z } from 'zod'
import pick from 'lodash/pick'

const getOtherUserProfileRouter = express.Router()

getOtherUserProfileRouter.get(
  '/api/users/profile/:otherUserId',
  currentUser,
  requireAuth,
  validateParams(z.object({ otherUserId: z.string() }), async (req, res) => {
    const { otherUserId } = req.params

    const group = await Group.findById(req.currentUser!.groups[0])

    if (!group || group.users.length < 1) {
      throw new BadRequestError('Unknown Group')
    }

    const isGroupedTogether = group.users.includes(otherUserId)

    let userProfile = await UserProfile.findById(otherUserId)

    if (!userProfile) {
      throw new NotFoundError()
    }

    if (!isGroupedTogether) {
      // @ts-ignore
      userProfile = pick(userProfile, ['id', 'displayName', 'avatar'])
    }

    res.status(200).send(userProfile)
  })
)

export { getOtherUserProfileRouter }
