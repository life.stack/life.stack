import express from 'express'
import { UserProfile } from '../../models/userProfile'
import { currentUser, validateBody } from '@life.stack/common'
import { requireAuthButNotGroup } from '../../middleware/requireAuthButNotGroup'
import { UserProfileSchema } from '@life.stack/common/build/schemas'

const createUserProfilesRouter = express.Router()

createUserProfilesRouter.post(
  '/api/users/profile',
  currentUser,
  requireAuthButNotGroup,
  validateBody(UserProfileSchema, async (req, res) => {
    let userProfile = await UserProfile.findById(req.currentUser!.id)

    if (!userProfile) {
      userProfile = new UserProfile({ _id: req.currentUser!.id })
    }

    userProfile.set(req.body)
    userProfile.save()

    res.status(200).send(userProfile)
  })
)

export { createUserProfilesRouter }
