import request from 'supertest'
import { app } from '../../../app'
import mongoose from 'mongoose'

it('responds with details about the current user', async () => {
  const userId = new mongoose.Types.ObjectId()
  const cookie = await global.signup(userId)

  const response = await request(app)
    .get('/api/users/currentuser')
    .set('Cookie', cookie)
    .send()
    .expect(200)

  expect(response.body.currentUser.email).toEqual(`${userId}@test.com`)
})

it('responds with null if not authenticated', async () => {
  const response = await request(app)
    .get('/api/users/currentuser')
    .send()
    .expect(200)

  expect(response.body.currentUser).toEqual(null)
})
