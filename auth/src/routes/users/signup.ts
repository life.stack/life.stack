import express, { Request, Response } from 'express'
const router = express.Router()
import { body } from 'express-validator'
import { User } from '../../models/user'
import { validateRequest, BadRequestError } from '@life.stack/common'
import generateJWT from '../../services/generateJWT'
import { UserProfile } from '../../models/userProfile'

router.post(
  '/api/users/signup',
  [
    body('displayName').isString().isLength({ min: 1, max: 128 }),
    body('email').isEmail().withMessage('Email must be valid'),
    body('password')
      .trim()
      .isStrongPassword({
        minSymbols: 0,
        minUppercase: 0,
        minNumbers: 0,
        minLength: 6,
      })
      .withMessage('Password must be strong'),
  ],
  validateRequest,
  async (req: Request, res: Response) => {
    const { email, password, displayName } = req.body

    const existingUser = await User.findOne({ email })

    if (existingUser) {
      // console.error('Email in use', email)
      throw new BadRequestError('Email in use')
    }

    const user = User.build({ email, password })
    const save = await user.save()

    const profile = new UserProfile({ _id: save.id, displayName })
    await profile.save()

    // Generate JWT
    const userJwt = generateJWT(user)

    // Store JWT on the session
    req.session = { jwt: userJwt }

    res.status(201).send(user)
  }
)

export { router as signupRouter }
