import { z } from 'zod'

export const UserSchema = z.object({
  id: z.string(),
  email: z.string().email(),
  groups: z.array(z.string()),
  iat: z.number(),
})

export interface User extends z.infer<typeof UserSchema> {}
