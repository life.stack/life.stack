// src/lib/trpc/auth.ts
import { z } from 'zod'
import { publicProcedure, router } from '../trpc'
import { User } from '../models/user'
import { PasswordManager } from '../services/passwordManager'
import generateJWT from '../services/generateJWT'

export const usersRouter = router({
  signin: publicProcedure
    .input(
      z.object({
        email: z.string().email(),
        password: z.string(),
      })
    )
    .mutation(async ({ input }) => {
      const { email, password } = input
      const existingUser = await User.findOne({ email })
      if (!existingUser) {
        throw new Error('Authentication Denied')
      }

      const passwordsMatch = await PasswordManager.compare(
        existingUser.password,
        password
      )
      if (!passwordsMatch) {
        throw new Error('Authentication Denied')
      }

      // Generate JWT
      const userJwt = generateJWT(existingUser)

      // Normally, you would set the JWT in a cookie here, but since tRPC
      // doesn't handle HTTP directly, you'll need to return the JWT and handle
      // the cookie in the client or through a server-side adapter.
      return { jwt: userJwt, user: existingUser }
    }),

  signout: publicProcedure.mutation(async ({ ctx }) => {
    // Invalidate the session here. You'll need to handle session invalidation
    // on the client or through a server-side adapter since tRPC doesn't handle
    // HTTP directly.
    ctx.session = null
    return { success: true }
  }),

  currentUser: publicProcedure.mutation(async ({ ctx }) => {
    // Assuming you have a way to get the current user from the context
    return { currentUser: ctx.currentUser || null }
  }),
})
