import express, { Request } from 'express'
import 'express-async-errors'
import { json } from 'body-parser'
import cookieSession from 'cookie-session'
import { currentuserRouter } from './routes/users/currentuser'
import { signinRouter } from './routes/users/signin'
import { signoutRouter } from './routes/users/signout'
import { signupRouter } from './routes/users/signup'
import { errorHandler, livenessRouter, NotFoundError } from '@life.stack/common'
import { createGroupRouter } from './routes/groups/newGroup'
import { currentgroupsRouter } from './routes/groups/currentgroups'
import { acceptGroupRouter } from './routes/groups/accept'
import { inviteGroupRouter } from './routes/groups/invite'
import { createUserProfilesRouter } from './routes/userProfiles/createUserProfile'
import { getCurrentUserProfileRouter } from './routes/userProfiles/getCurrentUserProfile'
import { showGroupRouter } from './routes/groups/showGroup'
import { getOtherUserProfileRouter } from './routes/userProfiles/getOtherUserProfile'
import { kickUserFromGroupRouter } from './routes/groups/kickUserFromGroup'
import { addOwnerToGroup } from './routes/groups/addOwnerToGroup'

const app = express()
app.set('trust proxy', true) // App is behind nginx proxy and should trust it
app.use(json())
app.use(
  cookieSession({
    signed: false,
    secure: process.env.NODE_ENV !== 'test',
  })
)

// /api/users
app.use(currentuserRouter)
app.use(signinRouter)
app.use(signoutRouter)
app.use(signupRouter)

// /api/usersProfile
app.use(createUserProfilesRouter)
app.use(getCurrentUserProfileRouter)
app.use(getOtherUserProfileRouter)

// /api/groups
app.use(createGroupRouter)
app.use(currentgroupsRouter)
// app.use(joinGroupRouter) // Should there be a join group? TBD
app.use(inviteGroupRouter)
app.use(acceptGroupRouter)
app.use(showGroupRouter)
app.use(kickUserFromGroupRouter)
app.use(addOwnerToGroup)

// health
app.use(livenessRouter)

// If real routes don't work, throw a not found
app.all('*', (req: Request) => {
  console.log('Route not found: ', req.originalUrl)
  throw new NotFoundError()
})

// Error Handler
app.use(errorHandler)

export { app }
