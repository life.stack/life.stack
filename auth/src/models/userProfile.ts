import mongoose from 'mongoose'
import { updateIfCurrentPlugin } from 'mongoose-update-if-current'
import { UserProfile } from '@life.stack/common/build/schemas'

// an interface that describes the properties a user document
export interface UserProfileDoc extends UserProfile, mongoose.Document {}

const userProfileSchema = new mongoose.Schema(
  {},
  {
    strict: false,
    toJSON: {
      versionKey: false,
      transform(doc, ret) {
        ret.id = ret._id
        delete ret._id
      },
    },
  }
)

userProfileSchema.set('versionKey', 'version')
userProfileSchema.plugin(updateIfCurrentPlugin)

const UserProfile = mongoose.model<UserProfileDoc>(
  'UserProfile',
  userProfileSchema
)

export { UserProfile }
