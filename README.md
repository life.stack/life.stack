# Life.Stack

Track your life's todo list and calendar with this open source web app.


## Description


Keep a list of things

Schedule a calendar of events

Keep track of those anniversaries

## Development

Install 
- Skaffold: https://skaffold.dev/docs/install/
- Kubectl: https://kubernetes.io/docs/tasks/tools/
  - https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/#install-using-native-package-management
  - echo 'source <(kubectl completion bash)' >>~/.bashrc
- Minikube: https://minikube.sigs.k8s.io/docs/start/
  - https://kubernetes.io/docs/tasks/access-application-cluster/ingress-minikube/
- Make a test development cert for the cluster
  - https://github.com/FiloSottile/mkcert
  - https://minikube.sigs.k8s.io/docs/tutorials/custom_cert_ingress/




First time setup requires configure a host entry for our development domain to our minikube ip.
We also need to install the nginx Ingress Controller into minikube.
```
$ minikube start
$ echo -e "`minikube ip`\lifestack.test" | sudo tee -a /etc/hosts
$ mkcert -cert-file cert.pem -key-file key.pem lifestack.test "*.lifestack.test" $(minikube ip)
$ kubectl -n kube-system create secret tls mkcert --key ./key.pem --cert ./cert.pem
$ minikube addons configure ingress
-- Enter custom cert(format is "namespace/secret"): kube-system/mkcert
✅  ingress was successfully configured

$ minikube addons enable ingress
$ minikube addons enable ingress-dns
$ open https://minikube.sigs.k8s.io/docs/handbook/addons/ingress-dns/#linux-os-with-network-manager
$ echo "TL;DR I used dnsmasq to forward to upstream dns and minikube"
```
Minikube will remain configured on your system.  For proceeding sessions:
```
minikube start
skaffold dev
```

-------------------

## Author
Michael Ski