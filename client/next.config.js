// for transpiling all ESM @fullcalendar/* packages
// also, for piping fullcalendar thru babel (to learn why, see babel.config.js)
// const withTM = require('next-transpile-modules')([
//   '@fullcalendar/react',
//   '@fullcalendar/common',
//   '@fullcalendar/daygrid',
// ])
//
// module.exports = withTM({
//   experimental: {
//     esmExternals: false,
//   },
//   webpackDevMiddleware: (config) => {
//     config.watchOptions.poll = 300
//     return config
//   },
// })

module.exports = {
  webpackDevMiddleware: (config) => {
    config.watchOptions.poll = 300
    return config
  },
}
