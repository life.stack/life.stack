import { useRouter } from 'next/router'
import { Flex, useColorModeValue } from '@chakra-ui/react'
import CheckList from '../../../components/lists/CheckList'
import LoadingCircle from '../../../components/util/LoadingCircle'

const ListShow = ({ list }) => {
  const router = useRouter()
  const { listId } = router.query

  return (
    <Flex
      align="center"
      justify="center"
      bg={useColorModeValue('gray.50', 'gray.800')}
    >
      {listId ? <CheckList listId={listId} /> : <LoadingCircle />}
    </Flex>
  )
}

// export async function getServerSideProps(context) {
//   const { listId } = context.query
//   const data = await serverFetcher({
//     uri: `/api/lists2/${listId}`,
//     req: context.req,
//   })
//   // console.log('lists[id].index', data)
//
//   return {
//     props: { list: data }, // will be passed to the page component as props
//   }
// }

export default ListShow
