import React from 'react'
import {
  Button,
  Flex,
  Heading,
  Icon,
  Input,
  Stack,
  Text,
  useColorModeValue,
} from '@chakra-ui/react'
import { MdPlaylistAdd } from 'react-icons/md'
import { useForm } from 'react-hook-form'
import useLists from '../../api/useLists'

const NewList = () => {
  const { register, handleSubmit } = useForm()
  const { createList } = useLists()

  return (
    <Flex
      align="center"
      justify="center"
      py="1em"
      bg={useColorModeValue('gray.50', 'gray.800')}
    >
      <Stack
        boxShadow={'2xl'}
        bg={useColorModeValue('white', 'gray.700')}
        rounded={'xl'}
        p={10}
        spacing={8}
        align={'center'}
      >
        <Icon as={MdPlaylistAdd} w={24} h={24} />
        <Stack align={'center'} spacing={2}>
          <Heading
            textTransform={'uppercase'}
            fontSize={'3xl'}
            color={useColorModeValue('gray.800', 'gray.200')}
          >
            New List
          </Heading>
          <Text fontSize={'lg'} color={'gray.500'}>
            Give the list a name
          </Text>
        </Stack>
        <Stack
          as="form"
          onSubmit={handleSubmit(createList)}
          spacing={4}
          direction={{ base: 'column', md: 'row' }}
          width="full"
        >
          <Input
            {...register('name')}
            aria-label="give the list a name"
            type="text"
            placeholder="List Name"
            color={useColorModeValue('gray.800', 'gray.200')}
            bg={useColorModeValue('gray.100', 'gray.600')}
            rounded="full"
            border={0}
            _focus={{
              bg: useColorModeValue('gray.200', 'gray.800'),
              outline: 'none',
            }}
          />
          <Button
            type="submit"
            colorScheme="green"
            rounded="full"
            flex="1 0 auto"
            _hover={{ bg: 'blue.500' }}
            _focus={{ bg: 'blue.500' }}
          >
            Create
          </Button>
        </Stack>
      </Stack>
    </Flex>
  )
}

export async function getServerSideProps(context) {
  if (!context.req.headers.cookie) {
    return {
      props: {},
      redirect: {
        destination: '/auth/signin',
        permanent: false,
      },
    }
  }

  return {
    props: {},
  }
}

export default NewList
