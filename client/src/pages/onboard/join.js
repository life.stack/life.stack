import React from 'react'
import {
  Box,
  Button,
  Center,
  chakra,
  Flex,
  GridItem,
  Input,
  SimpleGrid,
  useColorModeValue,
  VisuallyHidden,
} from '@chakra-ui/react'
import { useRouter } from 'next/router'
import { useForm } from 'react-hook-form'
import { useGroups } from '../../api/useGroups'

const JoinGroup = () => {
  const router = useRouter()
  const { register, handleSubmit } = useForm()
  const { acceptGroup } = useGroups()

  return (
    <Box px={8} py={[0, 24]} mx="auto">
      <SimpleGrid
        alignItems="center"
        w={{ base: 'full', xl: 11 / 12 }}
        columns={{ base: 1, lg: 11 }}
        gap={{ base: 0, lg: 24 }}
        mx="auto"
      >
        <GridItem
          colSpan={{ base: 'auto', lg: 5 }}
          textAlign={{ base: 'center', lg: 'left' }}
        >
          <chakra.h1
            mb={4}
            fontSize={{ base: '3xl', md: '4xl' }}
            fontWeight="bold"
            lineHeight={{ base: 'shorter', md: 'none' }}
            color={useColorModeValue('gray.900', 'gray.200')}
            letterSpacing={{ base: 'normal', md: 'tight' }}
          >
            Ready to join an existing group?
          </chakra.h1>
          <chakra.p
            mb={{ base: 10, md: 4 }}
            fontSize={{ base: 'lg', md: 'xl' }}
            fontWeight="thin"
            color="gray.500"
            letterSpacing="wider"
          >
            Creating group goals can be very empowering and motivating.
            Achieving big changes or results is easier with a strong group of
            individuals working towards a shared aim, especially when there is
            shared vision and investment in doing so.
          </chakra.p>
        </GridItem>
        <GridItem colSpan={{ base: 'auto', md: 4 }}>
          <Box
            as="form"
            mb={6}
            rounded="lg"
            shadow="xl"
            onSubmit={handleSubmit(acceptGroup)}
          >
            <Center pb={0} color={useColorModeValue('gray.700', 'gray.600')}>
              <p>Enter your invite code</p>
            </Center>
            <SimpleGrid
              columns={1}
              px={6}
              py={4}
              spacing={4}
              borderBottom="solid 1px"
              borderColor={useColorModeValue('gray.200', 'gray.700')}
            >
              <Flex>
                <VisuallyHidden>Invite Code</VisuallyHidden>
                <Input
                  mt={0}
                  type="text"
                  placeholder="Invite Code"
                  required
                  {...register('inviteKey')}
                />
              </Flex>
              <Button colorScheme="brand" w="full" py={2} type="submit">
                Join Group
              </Button>
            </SimpleGrid>
            <Flex px={6} py={4}>
              <Button
                py={2}
                w="full"
                colorScheme="gray"
                onClick={() => router.push('/onboard/create')}
              >
                Create a Group
              </Button>
            </Flex>
          </Box>
        </GridItem>
      </SimpleGrid>
    </Box>
  )
}

export default JoinGroup
