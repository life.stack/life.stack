// import '@fullcalendar/common/main.css'
// import '@fullcalendar/daygrid/main.css'
// import '@fullcalendar/timegrid/main.css'
// import '@mobiscroll/react/dist/css/mobiscroll.min.css'
// import '../components/calendar/mobicalendar.css'

import { ChakraProvider, extendTheme } from '@chakra-ui/react'
import { GroupsProvider } from '../api/useGroups'
import Layout from '../components/foundation/Layout'
import ApiConfig from '../api/apiConfig'
import { UsersProvider } from '../api/useUsers'
import { CurrentUserProfileProvider } from '../api/useCurrentUserProfile'
import LoadingCircle from '../components/util/LoadingCircle'
import dynamic from 'next/dynamic'

const DynamicLayout = dynamic(() => import('../components/foundation/Layout'), {
  loading: () => <LoadingCircle />,
  ssr: true,
})

const colors = {
  brand: {
    50: '#ecefff',
    100: '#cbceeb',
    200: '#a9aed6',
    300: '#888ec5',
    400: '#666db3',
    500: '#4d5499',
    600: '#3c4178',
    700: '#2a2f57',
    800: '#181c37',
    900: '#080819',
  },
  lightBg: '#F9FAFB',
}
const config = {
  initialColorMode: 'dark',
  useSystemColorMode: true,
}

const theme = extendTheme({ colors, config })

const AppComponent = ({ Component, pageProps }) => {
  return (
    <ChakraProvider theme={theme}>
      <ApiConfig>
        <UsersProvider>
          <CurrentUserProfileProvider>
            <GroupsProvider>
              <DynamicLayout>
                <Component {...pageProps} />
              </DynamicLayout>
            </GroupsProvider>
          </CurrentUserProfileProvider>
        </UsersProvider>
      </ApiConfig>
    </ChakraProvider>
  )
}

export default AppComponent
