import React from 'react'
import { useColorModeValue, Wrap } from '@chakra-ui/react'
import CreateCalendarForm from '../../components/calendar/CreateCalendarForm'

const NewCalendarPage = () => {
  return (
    <Wrap
      align="center"
      justify="space-around"
      bg={useColorModeValue('gray.50', 'gray.800')}
    >
      <CreateCalendarForm />
    </Wrap>
  )
}

export async function getServerSideProps(context) {
  if (!context.req.headers.cookie) {
    return {
      props: {},
      redirect: {
        destination: '/auth/signin',
        permanent: false,
      },
    }
  }

  return {
    props: {},
  }
}

export default NewCalendarPage
