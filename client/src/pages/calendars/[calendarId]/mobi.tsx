import CalendarOverview from '../../../components/calendar/CalendarOverview'
import serverFetcher from '../../../api/serverFetcher'
import { Box, Heading, useColorModeValue } from '@chakra-ui/react'
import { useRouter } from 'next/router'
import useCalendar from '../../../api/useCalendar'
import MobiCalendar from '../../../components/calendar/MobiCalendar'

const CalendarShowMobiPage = ({ initialCalendar }) => {
  const router = useRouter()
  const { calendarId } = router.query

  const { calendar } = useCalendar(calendarId, initialCalendar)

  return (
    <Box
      // align="center"
      // justify="center"
      // width="100%"
      // height="100%"
      bg={useColorModeValue('gray.50', 'gray.800')}
    >
      <MobiCalendar />
    </Box>
  )
}

export async function getServerSideProps(context) {
  const { calendarId } = context.params
  const initialCalendar = await serverFetcher({
    uri: `/api/calendars/${calendarId}`,
    req: context.req,
  })

  return {
    props: { initialCalendar }, // will be passed to the page component as props
  }
}

export default CalendarShowMobiPage
