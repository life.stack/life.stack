import serverFetcher from '../../api/serverFetcher'
import { Heading, ListItem, UnorderedList } from '@chakra-ui/react'

const orderIndex = ({ orders = [] }) => {
  return (
    <div>
      <Heading>My Orders</Heading>
      <OrderList orders={orders} />
    </div>
  )
}

const OrderList = ({ orders }) => {
  return (
    <UnorderedList>
      {orders.map((order) => {
        return (
          <ListItem key={order.id}>
            {order.ticket.title} - {order.status}
          </ListItem>
        )
      })}
    </UnorderedList>
  )
}
export async function getServerSideProps(context) {
  const data = await serverFetcher({
    uri: `/api/orders`,
    req: context.req,
  })

  console.log('orders', data)
  return {
    props: { orders: data || [] }, // will be passed to the page component as props
  }
}

export default orderIndex
