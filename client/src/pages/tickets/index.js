import Link from 'next/link'
import serverFetcher from '../../api/serverFetcher'
import React from 'react'
import {
  Button,
  chakra,
  Flex,
  Heading,
  SimpleGrid,
  Stack,
  useColorModeValue,
} from '@chakra-ui/react'

const IndexPage = ({ currentUser, tickets }) => {
  return (
    <div>
      <Heading as="h2">Tickets</Heading>
      <Flex w="full" p={50} alignItems="center" justifyContent="center">
        <Stack
          direction={{ base: 'column' }}
          w="full"
          bg={{ md: useColorModeValue('white', 'gray.800') }}
          shadow="lg"
        >
          <SimpleGrid
            spacingY={3}
            columns={{ base: 1, md: 3 }}
            w={{ base: 120, md: 'full' }}
            textTransform="uppercase"
            bg={useColorModeValue('gray.100', 'gray.700')}
            color={useColorModeValue('gray.500', 'gray.500')}
            py={{ base: 1, md: 4 }}
            px={{ base: 2, md: 10 }}
            fontSize="md"
            fontWeight="hairline"
          >
            <chakra.span>Title</chakra.span>
            <chakra.span>Price</chakra.span>
          </SimpleGrid>
          <TicketList tickets={tickets} />
        </Stack>
      </Flex>
    </div>
  )
}

const TicketList = ({ tickets = [] }) => {
  const bg = useColorModeValue('white', 'gray.800')

  const ticketList = tickets.map((ticket, index) => {
    return (
      <Flex direction={{ base: 'row', md: 'column' }} bg={bg} key={ticket.id}>
        <SimpleGrid
          spacingY={3}
          columns={{ base: 1, md: 3 }}
          w="full"
          py={2}
          px={10}
          fontWeight="hairline"
        >
          <span>{ticket.title}</span>
          <chakra.span
            textOverflow="ellipsis"
            overflow="hidden"
            whiteSpace="nowrap"
          >
            {ticket.price}
          </chakra.span>
          <Flex justify={{ md: 'end' }}>
            <Link
              href="/tickets/[ticketId]"
              as={`/tickets/${ticket.id}`}
              passHref
            >
              <Button variant="solid" colorScheme="blue" size="sm">
                Details
              </Button>
            </Link>
          </Flex>
        </SimpleGrid>
      </Flex>
    )
  })

  return ticketList
}

// During SSR these will fill in props of the component for it's single render
// IndexPage.getInitialProps = async (context, client, currentUser) => {
//   const { data } = await client.get('/api/tickets')
//
//   return { tickets: data }
// }

// During SSR these will fill in props of the component for it's single render
export const getServerSideProps = async (context) => {
  let props = {}
  try {
    // console.log(context.req.headers)
    const data = await serverFetcher({
      req: context.req,
      uri: '/api/tickets',
    })
    // console.log('getServerSideProps', data)

    props = { tickets: data }
  } catch (error) {
    console.error('tickets getServerSideProps error', error)
  }

  return { props: props }
}

export default IndexPage
