import { useState } from 'react'
import useRequest from '../../hooks/useRequest'
import Router from 'next/router'
import {
  Button,
  FormControl,
  FormHelperText,
  FormLabel,
  Heading,
  Input,
} from '@chakra-ui/react'

const NewTicket = () => {
  const [title, setTitle] = useState('')
  const [price, setPrice] = useState('')
  const { doRequest, errors } = useRequest({
    url: '/api/tickets',
    method: 'post',
    body: {
      title,
      price,
    },
    onSuccess: () => Router.push('/tickets'),
  })

  const onSubmit = (event) => {
    event.preventDefault()
    doRequest()
  }

  const onBlur = () => {
    const value = parseFloat(price)
    if (isNaN(value)) {
      return
    }
    setPrice(value.toFixed(2))
  }

  return (
    <div>
      <Heading as="h1">Create a Ticket</Heading>
      <form onSubmit={onSubmit}>
        <FormControl id="title" isRequired>
          <FormLabel>Title</FormLabel>
          <Input value={title} onChange={(e) => setTitle(e.target.value)} />
          <FormHelperText>What are you selling?</FormHelperText>
        </FormControl>
        <FormControl id="price" isRequired>
          <FormLabel>Price</FormLabel>
          <Input
            value={price}
            onChange={(e) => setPrice(e.target.value)}
            onBlur={onBlur}
          />
          <FormHelperText>
            How much are you asking? Must be at least $0.25
          </FormHelperText>
        </FormControl>
        {errors}
        <Button type="submit" colorScheme="blue">
          Submit
        </Button>
      </form>
    </div>
  )
}

export default NewTicket
