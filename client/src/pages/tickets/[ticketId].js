import serverFetcher from '../../api/serverFetcher'
import useRequest from '../../hooks/useRequest'
import { useRouter } from 'next/router'
import { Button, Text } from '@chakra-ui/react'

const TicketShow = ({ ticket }) => {
  const router = useRouter()
  console.log('ticketId', ticket.id)

  const { doRequest, errors } = useRequest({
    url: '/api/orders',
    method: 'post',
    body: {
      ticketId: ticket.id,
    },
    onSuccess: (order) =>
      router.push(`/orders/[orderId]`, `/orders/${order.id}`),
  })

  return (
    <>
      <Text fontSize="lg">{ticket.title}</Text>
      <Text fontSize="md">Price: ${ticket.price}</Text>
      {errors}
      <Button color="blue" onClick={() => doRequest()}>
        Purchase
      </Button>
    </>
  )
}

export async function getServerSideProps(context) {
  const { ticketId } = context.query
  const data = await serverFetcher({
    uri: `/api/tickets/${ticketId}`,
    req: context.req,
  })

  return {
    props: { ticket: data }, // will be passed to the page component as props
  }
}

export default TicketShow
