import React from 'react'
import {
  Box,
  GridItem,
  Heading,
  SimpleGrid,
  Skeleton,
  Text,
  useColorModeValue,
} from '@chakra-ui/react'
import GroupInformation from 'components/groups/GroupInformation'
import GroupInvitation from 'components/groups/GroupInvitation'
import GroupUserManagement from 'components/groups/GroupUserManagement'
import { useUsers } from '../../api/useUsers'
import LoadingCircle from 'components/util/LoadingCircle'
import TwoPanelDivider from 'components/layout/TwoPanelDivider'
import useGroup from '../../api/useGroup'
import { useGroups } from '../../api/useGroups'

function ManageGroups({}) {
  const { user, currentGroup } = useUsers()

  const { group, updateGroup } = useGroup(currentGroup)
  const descriptionColor = useColorModeValue('gray.600', 'gray.400')
  const bgColor = useColorModeValue('gray.50', 'inherit')
  console.log('manage', user && user.groups.length > 0)
  console.log('ManageGroups', group)

  return user && group ? (
    <Box bg={bgColor} p={[null, 10]}>
      <Box>
        <SimpleGrid
          display={{ base: 'initial', md: 'grid' }}
          columns={{ md: 3 }}
          spacing={{ md: 6 }}
        >
          <GridItem colSpan={{ md: 1 }}>
            <Box px={[0, 2, 4]}>
              <Heading fontSize="lg" fontWeight="md" lineHeight="6">
                Group Information
              </Heading>
              <Text mt={1} fontSize="sm" color={descriptionColor}>
                General information regarding the group
              </Text>
            </Box>
          </GridItem>
          <GridItem mt={[5, null, 0]} colSpan={{ md: 2 }}>
            <Skeleton isLoaded={Boolean(user)}>
              <GroupInformation group={group} updateGroup={updateGroup} />
            </Skeleton>
          </GridItem>
        </SimpleGrid>
      </Box>

      <TwoPanelDivider />

      <Box mt={[5, 0]}>
        <SimpleGrid
          display={{ base: 'initial', md: 'grid' }}
          columns={{ md: 3 }}
          spacing={{ md: 6 }}
        >
          <GridItem colSpan={{ md: 1 }}>
            <Box px={[4, 0]}>
              <Heading fontSize="lg" fontWeight="medium" lineHeight="6">
                Invitations
              </Heading>
              <Text mt={1} fontSize="sm" color={descriptionColor}>
                Invite users to the group
              </Text>
            </Box>
          </GridItem>
          <GridItem mt={[5, null, 0]} colSpan={{ md: 2 }}>
            <GroupInvitation />
          </GridItem>
        </SimpleGrid>
      </Box>

      <TwoPanelDivider />

      <Box mt={[5, 0]}>
        <SimpleGrid
          display={{ base: 'initial', md: 'grid' }}
          columns={{ md: 3 }}
          spacing={{ md: 6 }}
        >
          <GridItem colSpan={{ md: 1 }}>
            <Box px={[4, 0]}>
              <Heading fontSize="lg" fontWeight="medium" lineHeight="6">
                User Management
              </Heading>
              <Text mt={1} fontSize="sm" color={descriptionColor}>
                Manage the users in this group
              </Text>
            </Box>
          </GridItem>
          <GridItem mt={[5, null, 0]} colSpan={{ md: 2 }}>
            <GroupUserManagement />
          </GridItem>
        </SimpleGrid>
      </Box>
    </Box>
  ) : (
    <LoadingCircle />
  )
}

// During SSR these will fill in props of the component for it's single render
// export const getServerSideProps: GetServerSideProps = async (context) => {
//   const user = parseUserSession(context)
//
//   if (!user) {
//     return {
//       redirect: {
//         destination: '/signin',
//         permanent: false,
//       },
//     }
//   }
//
//   return {
//     props: {
//       user: user,
//       currentGroupId: user.groups[0],
//     },
//   }
// }
//
// function parseUserSession(context: GetServerSidePropsContext) {
//   try {
//     const expressSession = context.req.cookies['express:sess']
//     if (expressSession) {
//       const parsedObj = JSON.parse(atob(expressSession))
//       const jwt = parseJwt(parsedObj.jwt)
//       console.log(jwt)
//       return jwt
//     }
//   } catch (e) {
//     return null
//   }
// }
//
// function parseJwt(token) {
//   try {
//     const base64Url = token.split('.')[1]
//     const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/')
//     const jsonPayload = decodeURIComponent(
//       atob(base64)
//         .split('')
//         .map(function (c) {
//           return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
//         })
//         .join('')
//     )
//
//     return JSON.parse(jsonPayload)
//   } catch (e) {
//     return null
//   }
// }

export default ManageGroups
