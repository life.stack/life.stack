import AuthForm from 'components/auth/AuthForm'
import { Heading, Link, Stack, Text } from '@chakra-ui/react'
import { useUsers } from '../../api/useUsers'

const Signup = () => {
  const { signup } = useUsers()

  return (
    <AuthForm signup onSubmit={signup}>
      <Stack align="center">
        <Heading fontSize="4xl">Sign up for an account</Heading>
        <Text fontSize="lg" color="gray.600">
          to enjoy all of our cool <Link color="blue.400">features</Link> ✌️
        </Text>
      </Stack>
    </AuthForm>
  )
}

export default Signup
