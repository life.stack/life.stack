import serverFetcher from '../../../api/serverFetcher'
import { Flex, Heading, useColorModeValue } from '@chakra-ui/react'
import { useRouter } from 'next/router'
import useTrackerEvents from '../../../api/useTrackerEvents'
import TrackerGraph from '../../../components/trackers/TrackerGraph'
import useTrackerToLine from '../../../hooks/useTrackerToLine'
import CreateTrackerEventForm from '../../../components/trackers/CreateTrackerEventForm'

const TrackerCategoryShowPage = ({ initialTrackerEvents, initialCategory }) => {
  const router = useRouter()
  const { categoryId } = router.query
  const { events, category, createEventRequest } = useTrackerEvents(
    categoryId,
    initialTrackerEvents,
    initialCategory
  )
  const { data, dateTickValues, axisBottomFormat } = useTrackerToLine({
    events,
    name: category.name,
  })

  return (
    <Flex
      direction="column"
      align="center"
      justify="center"
      width="100%"
      height="100%"
      px={[1, 20]}
      mr={[0, 100]}
      bg={useColorModeValue('gray.50', 'gray.800')}
    >
      <Heading>{category!.name}</Heading>
      <TrackerGraph
        data={data}
        unit={category!.unit}
        dateTickValues={dateTickValues}
        axisBottomFormat={axisBottomFormat}
      />
      <CreateTrackerEventForm createEventRequest={createEventRequest} />
    </Flex>
  )
}

export async function getServerSideProps(context) {
  const { categoryId } = context.query
  const initialTrackerEvents = await serverFetcher({
    uri: `/api/trackers/${categoryId}/events`,
    req: context.req,
  })
  const initialCategory = await serverFetcher({
    uri: `/api/trackers/${categoryId}`,
    req: context.req,
  })

  return {
    props: { initialTrackerEvents, initialCategory }, // will be passed to the page component as props
  }
}

export default TrackerCategoryShowPage
