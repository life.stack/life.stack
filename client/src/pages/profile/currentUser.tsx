import UserProfile from '../../components/auth/UserProfile'
import { useUsers } from '../../api/useUsers'
import LoadingCircle from '../../components/util/LoadingCircle'

const CurrentUser = () => {
  const { user } = useUsers()

  return user ? <UserProfile /> : <LoadingCircle />
}

export default CurrentUser
