import useSWR from 'swr'
import useRequest from '../hooks/useRequest'
import { useRouter } from 'next/router'
import { useToast } from '@chakra-ui/react'
import { createContext, useContext } from 'react'
import { UserProfile } from '@life.stack/common/build/schemas'
import { useUsers } from './useUsers'

export const CurrentUserProfileContext = createContext(
  {} as CurrentUserProfileContextType
)

export type CurrentUserProfileContextType = {
  userProfile: UserProfile
  isLoading: boolean
  isError: boolean
  updateUserProfile: (props: UserProfile) => Promise<void>
}

export const useCurrentUserProfile = () => useContext(CurrentUserProfileContext)

export const CurrentUserProfileProvider = ({ children }) => {
  const { hasUser } = useUsers()
  const {
    data: userProfile,
    error,
    mutate,
  } = useSWR('/api/users/profile', {
    revalidateOnFocus: false,
    shouldRetryOnError: hasUser,
    onError: (err) => {
      console.warn('UserProfileProvider.onError', err)
    },
  })

  const { doRequest, errors } = useRequest({})

  // console.log('UserProfileProvider', userProfile)

  const updateUserProfile = async (updatedProfile: UserProfile) => {
    console.log('updateUserProfile', updatedProfile)
    mutate(updatedProfile)

    await doRequest({
      url: `/api/users/profile`,
      method: 'post',
      body: updatedProfile,
      onSuccess: (data) => {
        console.log('updateUserProfile.success', data)
        mutate(data)
      },
    })
  }

  return (
    <CurrentUserProfileContext.Provider
      value={{
        userProfile,
        isLoading: !error && !userProfile,
        isError: error,
        updateUserProfile,
      }}
    >
      {children}
    </CurrentUserProfileContext.Provider>
  )
}
