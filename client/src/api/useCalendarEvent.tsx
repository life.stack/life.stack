import useSWR, { useSWRConfig } from 'swr'
import useRequest from '../hooks/useRequest'
import { useToast } from '@chakra-ui/react'
import { useRouter } from 'next/router'
import { isArray } from '@chakra-ui/utils'
import { isPlainObject } from 'lodash'

const useCalendarEvent = (calendarId, initialCalendar?) => {
  // const { mutate: swrMutate } = useSWRConfig()
  const {
    data: calendar,
    error: calendarError,
    mutate: calendarMutate,
  } = useSWR(`/api/calendars/${calendarId}`, { fallbackData: initialCalendar })

  const { doRequest, errors } = useRequest({})

  console.log('useCalendarEvent', calendar, errors)

  const createCalendarEvent = async ({
    title,
    start,
    end,
    location,
    allDay,
  }) => {
    console.log('createEventRequest', title, start)
    await doRequest({
      url: `/api/calendars/${calendar.id}/events`,
      method: 'post',
      body: {
        title,
        start,
        end,
        location,
        allDay,
      },
      onSuccess: (data) => {
        console.log('createCalendarEvent.success', data)
        // swrMutate(`/api/calendars/${calendarId}/events`)
      },
    })
  }

  const updateCalendarEvent = async ({ event }) => {
    console.log('updateCalendarEvent', event)
    await doRequest({
      url: `/api/calendars/${calendar.id}/events/${event.id}`,
      method: 'put',
      body: event.toPlainObject({ collapseExtendedProps: true }),
      onSuccess: (data) => {
        console.log('updateCalendarEvent.success', data)
      },
    })
  }

  const removeCalendarEvent = async (event) => {
    console.log('removeCalendarEvent', event)
    await doRequest({
      url: `/api/calendars/${calendar.id}/events/${event.id}`,
      method: 'delete',
      onSuccess: (data) => {
        console.log('removeCalendarEvent.success', data)
      },
    })
  }

  return {
    calendar: isPlainObject(calendar) ? calendar : {},
    createCalendarEvent,
    updateCalendarEvent,
    removeCalendarEvent,
    isLoading: !calendarError && !calendar,
    isError: calendarError,
  }
}

export default useCalendarEvent
