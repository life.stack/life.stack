import { useToast } from '@chakra-ui/react'
import useSWR from 'swr'
import useRequest from '../hooks/useRequest'
import { useRouter } from 'next/router'

const useItem = ({ itemId, initialItem }) => {
  const toast = useToast()
  const {
    data: item,
    error,
    mutate,
  } = useSWR(`/api/items/${itemId}`, {
    fallbackData: initialItem,
    onSuccess: async (data, key, config) => {
      console.log('item fetched', data)
    },
  })
  const router = useRouter()

  // console.log('useItem', item)

  const { doRequest: updateItemRequest } = useRequest({
    url: `/api/items/${itemId}`,
    method: 'put',
  })

  const updateItem = async (item) => {
    console.log('updateItemRequest', item)
    await updateItemRequest({
      body: {
        title: item.title,
        note: item.note,
        checked: item.checked,
        subitems: item.subitems,
      },
      onSuccess: async (updatedItem) => {
        await mutate(updatedItem, false)
      },
    })
  }

  const toggleCheck = async () => {
    await updateItemRequest({
      body: {
        title: item.title,
        note: item.note,
        checked: !item.checked,
        subItems: item.subItems,
      },
      onSuccess: async (updatedItem) => {
        await mutate(updatedItem, false)
      },
    })
  }

  const { doRequest: appendSubitemRequest } = useRequest({
    url: `/api/items/${itemId}/subitem`,
    method: 'post',
    onSuccess: async (appendedItem) => {
      await mutate(
        { ...item, subitems: [...item.subitems, appendedItem] },
        true
      )
    },
  })

  const appendSubitem = async ({ title }) => {
    console.log('appendSubitem', title)
    await appendSubitemRequest({
      body: { title: title },
    })
  }

  const { doRequest: updateSubitemRequest } = useRequest({
    method: 'put',
  })

  const toggleSubitemChecked = async ({ id, checked }) => {
    await mutate(
      {
        ...item,
        subitems: item.subitems.map((subitem) =>
          subitem.id === id ? { ...subitem, checked: !checked } : subitem
        ),
      },
      false
    )

    await updateSubitemRequest({
      url: `/api/subitems/${id}/checked`,
      body: {
        checked: !checked,
      },
    })
  }

  return {
    item,
    isLoading: !error && !item,
    isError: error,
    updateItem,
    toggleCheck,
    appendSubitem,
    toggleSubitemChecked,
  }
}

export default useItem
