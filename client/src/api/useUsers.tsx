import useSWR, { useSWRConfig } from 'swr'
import useRequest from '../hooks/useRequest'
import Router, { useRouter } from 'next/router'
import { useToast } from '@chakra-ui/react'
import { createContext, useContext } from 'react'

export const UsersContext = createContext({} as UsersContextType)

export type UsersContextType = {
  user: { id: string; email: string; groups: [string] }
  hasUser: boolean
  currentGroup: string
  isLoading: boolean
  isError: boolean
  signin: (props: { email: string; password: string }) => Promise<void>
  signup: (props: {
    email: string
    password: string
    displayName: string
  }) => Promise<void>
  signout: () => Promise<void>
}

export const useUsers = () => useContext(UsersContext)

export const UsersProvider = ({ children }) => {
  const toast = useToast()
  const { mutate: swrMutate } = useSWRConfig()
  const { data, error, mutate } = useSWR('/api/users/currentuser', {
    fallbackData: { currentUser: null },
    onError: () => {
      console.warn('UsersProvider.onError -- signing out')
      // router.push('/auth/signout')
    },
  })

  console.log('UsersProvider', data)

  const { doRequest, errors } = useRequest({})

  const signup = async ({ email, password, displayName }) => {
    await doRequest({
      url: '/api/users/signup',
      method: 'post',
      body: { email, password, displayName },
      onSuccess: (data) => {
        toast({
          title: 'What better place than here? what better time than now!',
        })
        mutate({ currentUser: data })
        Router.push('/onboard')
      },
    })
  }

  const signout = async () => {
    swrMutate('/api/groups/currentGroups', undefined, false)
    swrMutate('/api/users/profile', undefined, false)
    mutate({ currentUser: null }, false)

    await doRequest({
      url: '/api/users/signout',
      method: 'post',
      body: {},
      onSuccess: async () => {
        await Router.push('/')
      },
    })
  }

  const signin = async ({ email, password }) => {
    await doRequest({
      url: '/api/users/signin',
      method: 'post',
      body: { email, password },
      onSuccess: async (data) => {
        toast({
          title: 'Welcome Back!',
        })
        await mutate({ currentUser: data }, false)
        await swrMutate('/api/groups/currentGroups', undefined, true)
        await swrMutate('/api/users/profile', undefined, true)
        await Router.push('/')
      },
    })
  }

  return (
    <UsersContext.Provider
      value={{
        user: data?.currentUser,
        hasUser: Boolean(data?.currentUser),
        currentGroup: data?.currentUser?.groups[0],
        isLoading: !error && data === undefined,
        isError: error || errors,
        signout: signout,
        signin: signin,
        signup: signup,
      }}
    >
      {children}
    </UsersContext.Provider>
  )
}
