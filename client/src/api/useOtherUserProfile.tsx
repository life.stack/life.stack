import useSWR from 'swr'
import useRequest from '../hooks/useRequest'
import { useRouter } from 'next/router'
import { useToast } from '@chakra-ui/react'
import { UserProfile } from '@life.stack/common/build/schemas'
import { SWRResponse } from 'swr/dist/types'
import { ZodError } from 'zod'

export type useOtherUserProfile = {
  userProfile: UserProfile
  isLoading: boolean
  isError: boolean
}

export const useOtherUserProfile = (userId): useOtherUserProfile => {
  const {
    data: userProfile,
    error,
    mutate,
  }: SWRResponse<UserProfile, [ZodError]> = useSWR(
    `/api/users/profile/${userId}`,
    {
      fallbackData: {},
      revalidateOnFocus: false,
      onError: (err) => {
        console.warn('useOtherUserProfile.onError', err)
      },
    }
  )

  const { doRequest, errors } = useRequest({})

  // console.log(`useOtherUserProfile: ${userId}`, userProfile)

  return {
    userProfile,
    isLoading: !error && !userProfile,
    isError: Boolean(error),
  }
}

export default useOtherUserProfile
