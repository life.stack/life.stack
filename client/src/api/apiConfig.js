import { SWRConfig } from 'swr'
import fetcher from './fetcher'

const fallback = {
  '/api/lists': [],
}

function apiConfig({ children }) {
  return (
    <SWRConfig value={{ fetcher: fetcher, fallback: fallback }}>
      {children}
    </SWRConfig>
  )
}

export default apiConfig
