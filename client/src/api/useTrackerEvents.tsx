import useSWR, { useSWRConfig } from 'swr'
import useRequest from '../hooks/useRequest'
import { useToast } from '@chakra-ui/react'
import { useRouter } from 'next/router'
import { isArray } from '@chakra-ui/utils'

const useTrackerEvents = (
  categoryId,
  initialTrackerEvents,
  initialCategory
) => {
  const { mutate: swrMutate } = useSWRConfig()
  const toast = useToast()
  const router = useRouter()
  const {
    data: events,
    error: eventsError,
    mutate: eventsMutate,
  } = useSWR(`/api/trackers/${categoryId}/events`, {
    fallbackData: initialTrackerEvents,
  })
  const {
    data: category,
    error: categoryError,
    mutate: categoryMutate,
  } = useSWR(`/api/trackers/${categoryId}`, { fallbackData: initialCategory })

  const { doRequest, errors } = useRequest({})

  console.log('useTrackerEvents', events, errors)

  const createEventRequest = async ({ value, timestamp }) => {
    console.log('createEventRequest', value, timestamp)
    await doRequest({
      url: `/api/trackers/${category.id}`,
      method: 'post',
      body: {
        value,
        timestamp,
      },
      onSuccess: (data) => {
        console.log('createEventRequest.success', data)
        eventsMutate(events.push(data))
      },
    })
  }

  return {
    events: isArray(events) ? events : [],
    category,
    createEventRequest,
    isLoading: !eventsError && !categoryError && !events,
    isError: eventsError || categoryError,
  }
}

export default useTrackerEvents
