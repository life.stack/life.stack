const serverFetcher = ({ uri, req, ...args }) =>
  fetch(`${process.env.BASE_URL}${uri}`, {
    headers: {
      cookie: req.headers.cookie,
    },
    ...args,
  }).then((res) => {
    // console.log('res', res)
    if (res.status >= 200 && res.status < 300) {
      return res.json()
    }
    return null
  })

export default serverFetcher
