import { useToast } from '@chakra-ui/react'
import useSWR from 'swr'
import useRequest from '../hooks/useRequest'
import { useRouter } from 'next/router'
import { SWRResponse } from 'swr/dist/types'
import { useGroups } from './useGroups'

type ListObject = { id: string; name: string }

const useLists = () => {
  const toast = useToast()
  const {
    data: lists,
    error,
    mutate,
  }: SWRResponse<Array<ListObject>, [{ message: string }]> = useSWR(
    '/api/lists',
    {}
  )
  const router = useRouter()

  // console.log('useLists', lists)

  const { doRequest: createListRequest } = useRequest({
    url: '/api/lists',
    method: 'post',
    onSuccess: async (createdList: ListObject) => {
      toast({
        title: 'List Created! 🎉',
      })
      await mutate([...lists, createdList], true)
      await router.push('/lists/[listId]', `/lists/${createdList.id}`)
    },
  })

  const createList = async ({ name }) => {
    console.log('Creating List', name)
    await createListRequest({ body: { name } })
  }

  return {
    lists: lists || [],
    navBarLists: Array.isArray(lists)
      ? lists.map((list) => ({
          id: list.id,
          name: list.name,
        }))
      : [],
    isLoading: !error && !lists,
    isError: error,
    createList,
  }
}

export default useLists
