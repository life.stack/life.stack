import { useToast } from '@chakra-ui/react'
import useSWR from 'swr'
import useRequest from '../hooks/useRequest'
import { useRouter } from 'next/router'
import { SWRResponse } from 'swr/dist/types'

type Calendar = {
  id: string
  name: string
  groupId: string
  version: number
}

const useCalendars = () => {
  const toast = useToast()
  const {
    data: calendars,
    error,
    mutate,
  }: SWRResponse<Array<Calendar>, [{ message: string }]> = useSWR(
    '/api/calendars'
  )
  const router = useRouter()

  // console.log('useCalendars', calendars)

  const { doRequest } = useRequest({})

  const createCalendar = async ({ name }) => {
    console.log('createCalendar', name)
    await doRequest({
      url: '/api/calendars',
      method: 'post',
      body: { name },
      onSuccess: async (createdCalendar: Calendar) => {
        toast({
          title: 'Calendar Created! 📅',
        })
        await mutate([...calendars, createdCalendar], true)
        await router.push(
          '/calendars/[calendarId]',
          `/calendars/${createdCalendar.id}`
        )
      },
    })
  }

  return {
    calendars: calendars || [],
    navBarCalendars: Array.isArray(calendars)
      ? calendars.map((calendar) => ({
          id: calendar.id,
          name: calendar.name,
        }))
      : [],
    isLoading: !error && !calendars,
    isError: error,
    createCalendar: createCalendar,
  }
}

export default useCalendars
