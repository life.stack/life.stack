import { useToast } from '@chakra-ui/react'
import useSWR from 'swr'
import useRequest from '../hooks/useRequest'
import { useRouter } from 'next/router'
import { SWRResponse } from 'swr/dist/types'

type TrackerCategory = {
  id: string
  name: string
  type: string
  unit: string
  groupId: string
  ownerId: string
  version: number
}

const useTrackers = () => {
  const toast = useToast()
  const {
    data: trackers,
    error,
    mutate,
  }: SWRResponse<Array<TrackerCategory>, [{ message: string }]> = useSWR(
    '/api/trackers'
  )
  const router = useRouter()

  // console.log('useTrackers', trackers)

  const { doRequest } = useRequest({})

  const createTrackerCategory = async ({ name, unit, type }) => {
    console.log('createTrackerCategory', name)
    await doRequest({
      url: '/api/trackers',
      method: 'post',
      body: { name, type, unit },
      onSuccess: async (createdCategory: TrackerCategory) => {
        toast({
          title: 'Tracker Created! 🎉',
        })
        await mutate([...trackers, createdCategory], true)
        await router.push(
          '/trackers/[categoryId]',
          `/trackers/${createdCategory.id}`
        )
      },
    })
  }

  return {
    calendars: trackers || [],
    navBarTrackers: Array.isArray(trackers)
      ? trackers.map((tracker) => ({
          id: tracker.id,
          name: tracker.name,
        }))
      : [],
    isLoading: !error && !trackers,
    isError: error,
    createTrackerCategory: createTrackerCategory,
  }
}

export default useTrackers
