import { useToast } from '@chakra-ui/react'
import useSWR from 'swr'
import useRequest from '../hooks/useRequest'
import { createContext, useContext, useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import { useUsers } from './useUsers'
import { SWRResponse } from 'swr/dist/types'
import { isArray } from 'lodash'

export const GroupsContext = createContext({} as useGroups)

type useGroups = {
  groups: [
    {
      id: string
      name: string
      owners: [string]
      users: [string]
    }?
  ]
  hasGroup: boolean
  currentGroup: {
    id: string
    name: string
    owners: [string]
    users: [string]
  }
  currentGroupId: string
  isLoading: boolean
  isError: boolean
  createGroup: Function
  inviteCode: string
  generateInvite: Function
  acceptGroup: Function
  clearGroups: Function
}

export const useGroups = (): useGroups => useContext(GroupsContext) as useGroups

export const GroupsProvider = ({ children }) => {
  const toast = useToast()
  const { hasUser } = useUsers()
  const {
    data: groups,
    error,
    mutate,
  }: SWRResponse<
    [{ id: string; name: string; owners: [string]; users: [string] }?],
    [{ message: string }]
  > = useSWR('/api/groups/currentGroups', {
    // fallbackData: [],
    revalidateOnReconnect: hasUser,
    revalidateOnFocus: hasUser,
    revalidateOnMount: hasUser,
    shouldRetryOnError: hasUser,
  })
  const [inviteCode, setInviteCode] = useState('')
  const router = useRouter()

  useEffect(() => {
    if (hasUser && !groups) {
      mutate(undefined, true)
    }
  }, [hasUser, groups, mutate])

  console.log('GroupsProvider', groups, hasUser)

  const { doRequest } = useRequest({})

  const createGroup = async ({ name }) => {
    console.log('Creating Group', name)

    const response = doRequest({
      url: '/api/groups',
      method: 'post',
      body: { name },
      onSuccess: async (createdGroup) => {
        toast({
          title: 'Group Created! 🎉',
        })
        await mutate([createdGroup], true)
        await router.push('/')
      },
    })
  }

  const joinGroup = () => {}

  const generateInvite = async () => {
    console.log('Generating invite...')
    const response = doRequest({
      url: `/api/groups/${groups[0]?.id}/invite`,
      method: 'post',
      onSuccess: (data) => {
        setInviteCode(data?.inviteKey)
        toast({
          title: 'Invite Code Generated',
        })
      },
    })
  }

  const acceptGroup = async ({ inviteKey }) => {
    console.log('Accepting Group...')
    const response = doRequest({
      url: `/api/groups/accept`,
      method: 'post',
      body: { inviteKey },
      onSuccess: async (response) => {
        toast({
          title: 'Invite Accepted!',
        })
        console.log('accepted group', response)
        await mutate([response], true)
        await router.push('/')
      },
    })
  }

  const clearGroups = async () => {
    await mutate([], true)
  }

  return (
    <GroupsContext.Provider
      value={{
        groups: hasUser ? groups : undefined,
        hasGroup: isArray(groups) && groups.length > 0,
        currentGroup: isArray(groups) ? groups[0] : null,
        currentGroupId: isArray(groups) ? groups[0]?.id : null,
        isLoading: !error && !Boolean(groups),
        isError: Boolean(error),
        createGroup,
        inviteCode,
        generateInvite,
        acceptGroup,
        clearGroups,
      }}
    >
      {children}
    </GroupsContext.Provider>
  )
}
