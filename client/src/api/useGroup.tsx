import useSWR, { useSWRConfig } from 'swr'
import useRequest from '../hooks/useRequest'
import { useState } from 'react'
import { useToast } from '@chakra-ui/react'
import { useRouter } from 'next/router'
import { SWRResponse } from 'swr/dist/types'
import { ZodError } from 'zod'
import { Group } from '../schemas/GroupSchema'
import { CurrentUser } from '@life.stack/common/build/schemas'

const useGroup = (groupId) => {
  const { mutate: swrMutate } = useSWRConfig()
  const toast = useToast()
  const router = useRouter()
  const {
    data: group,
    error,
    mutate,
  }: SWRResponse<Group, [ZodError]> = useSWR(`/api/groups/${groupId}`, {
    // fallbackData: null,
  })

  const { doRequest } = useRequest({})

  console.log('useGroup', group)

  const updateGroup = async (updatedGroup: Group) => {
    mutate(updatedGroup)

    await doRequest({
      url: `/api/groups/${groupId}/update`,
      method: 'post',
      body: updatedGroup,
      onSuccess: (data) => {
        console.log('updateGroup.success', data)
        mutate(data)
      },
    })
  }

  const kickUserFromGroup = async ({ id }: CurrentUser) => {
    mutate({ ...group, users: group.users.filter((u) => u !== id) })

    await doRequest({
      url: `/api/groups/${groupId}/kick`,
      method: 'post',
      body: { userId: id },
      onSuccess: (data) => {
        console.log('kickUserFromGroup.success', data)
        mutate(data)
      },
    })
  }

  const addOwnerToGroup = async ({ id }: CurrentUser) => {
    mutate({ ...group, owners: [...group.owners, id] })

    await doRequest({
      url: `/api/groups/${groupId}/owner`,
      method: 'post',
      body: { userId: id },
      onSuccess: (data) => {
        console.log('addOwnerToGroup.success', data)
        mutate(data)
      },
    })
  }

  return {
    group,
    isLoading: !error && !group,
    isError: error,
    updateGroup,
    kickUserFromGroup,
    addOwnerToGroup,
  }
}

export default useGroup
