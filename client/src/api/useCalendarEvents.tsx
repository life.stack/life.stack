import useSWR, { useSWRConfig } from 'swr'
import useRequest from '../hooks/useRequest'
import { useToast } from '@chakra-ui/react'
import { useRouter } from 'next/router'
import { isArray } from '@chakra-ui/utils'
import { isPlainObject } from 'lodash'
import { formatDate } from '@mobiscroll/react'

const useCalendarEvents = (
  calendarId,
  initialCalendarEvents?,
  initialCalendar?
) => {
  const { mutate: swrMutate } = useSWRConfig()
  const toast = useToast()
  const router = useRouter()
  const {
    data: events,
    error: eventsError,
    mutate: eventsMutate,
  } = useSWR(`/api/calendars/${calendarId}/events`, {
    fallbackData: initialCalendarEvents,
  })
  const {
    data: calendar,
    error: calendarError,
    mutate: calendarMutate,
  } = useSWR(`/api/calendars/${calendarId}`, { fallbackData: initialCalendar })

  const { doRequest, errors } = useRequest({})

  // console.log('useCalendarEvents', events, errors)

  const updateCalendarEvent = async (event) => {
    console.log('updateCalendarEvent', event)
    if (event.allDay) {
      event.start =
        typeof event.start === 'string'
          ? event.start
          : formatDate('MM/DD/YYYY', event.start)
      event.end = event.end
        ? typeof event.end === 'string'
          ? event.end
          : formatDate('MM/DD/YYYY', event.end)
        : undefined
      console.log('allDay', event.start, event.end)
    }
    await doRequest({
      url: `/api/calendars/${calendar.id}/events/${event.id}`,
      method: 'put',
      body: event,
      onSuccess: (data) => {
        console.log('updateCalendarEvent.success', data)
        eventsMutate(events.map((e) => (e.id === event.id ? data : e)))
      },
    })
  }

  const removeCalendarEvent = async (event) => {
    console.log('removeCalendarEvent', event)
    await doRequest({
      url: `/api/calendars/${calendar.id}/events/${event.id}`,
      method: 'delete',
      onSuccess: (data) => {
        console.log('removeCalendarEvent.success', data)
        eventsMutate(events.filter((e) => e.id === event.id))
      },
    })
  }

  // {
  //   title,
  //     start,
  //     end,
  //     location,
  //     allDay,
  //     description,
  //     status,
  //     color,
  //     recurring,
  // }

  const createCalendarEvent = async (newEvent) => {
    console.log(
      'createEventRequest',
      newEvent.title,
      newEvent.start,
      newEvent.end
    )
    if (newEvent.allDay) {
      newEvent.start = formatDate('MM/DD/YYYY', newEvent.start)
      newEvent.end = newEvent.end
        ? formatDate('MM/DD/YYYY', newEvent.end)
        : undefined
      console.log('allDay', newEvent.start, newEvent.end)
    }
    await eventsMutate([...events, newEvent])
    await doRequest({
      url: `/api/calendars/${calendar.id}/events`,
      method: 'post',
      body: newEvent,
      onSuccess: (data) => {
        console.log('createCalendarEvent.success', data)
        eventsMutate([...events, data])
        // swrMutate(`/api/calendars/${calendarId}/events`)
      },
    })
  }

  const refreshEvents = async () => {
    await eventsMutate(events, true)
  }

  return {
    events: isArray(events) ? events : [],
    calendar: isPlainObject(calendar) ? calendar : {},
    updateCalendarEvent,
    removeCalendarEvent,
    createCalendarEvent,
    isLoading: !eventsError && !calendarError && !events,
    isError: eventsError || calendarError,
    refreshEvents,
  }
}

export default useCalendarEvents
