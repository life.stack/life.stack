import { useState } from 'react'
import { useToast } from '@chakra-ui/react'

type useRequestProps = {
  url?: string
  method?: string
  body?: object
  onSuccess?: Function
}

//TODO: Get rid of these params!!!!!
const useRequest = ({ url, method, body, onSuccess }: useRequestProps) => {
  const [errors, setErrors] = useState(null)
  const toast = useToast()

  // console.log('useRequest', url, method, body)

  const doRequest = async (
    props: {
      body?: object
      headers?: object
      url?: string
      onSuccess?: Function
      method?: string
    } = {}
  ) => {
    try {
      const response = await fetch(props.url ? props.url : url, {
        method: props.method ? props.method : method,
        body: JSON.stringify(props.body ? props.body : body),
        headers: { 'Content-Type': 'application/json', ...props.headers },
      })
      const data = await response.json()
      // console.log('data', data)

      if (response.ok) {
        // console.log('response, ok')
        if (props.onSuccess) {
          props.onSuccess(data)
        } else {
          if (onSuccess) {
            onSuccess(data)
          }
        }
      } else {
        throw data
      }

      return data
    } catch (err) {
      console.error('useRequest.error', err)
      if (err.errors) {
        console.error(JSON.stringify(err))
        setErrors(err.errors)
        err.errors.forEach((anError) => {
          toast({
            title: anError.field,
            description: anError.message,
            status: 'error',
            isClosable: true,
            duration: 11000,
            variant: 'top-accent',
          })
        })
      }
    }
  }

  return { doRequest, errors }
}

export default useRequest
