import { Box, useColorModeValue } from '@chakra-ui/react'
import React from 'react'

const TwoPanelDivider = () => {
  return (
    <Box visibility={{ base: 'hidden', sm: 'visible' }} aria-hidden="true">
      <Box py={[1, 5]}>
        <Box
          borderTop="solid 1px"
          borderTopColor={useColorModeValue('gray.200', 'whiteAlpha.200')}
        />
      </Box>
    </Box>
  )
}

export default TwoPanelDivider
