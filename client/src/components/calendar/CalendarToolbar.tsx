import {
  Button,
  Flex,
  Heading,
  IconButton,
  VStack,
  Wrap,
  ButtonGroup,
} from '@chakra-ui/react'
import { useRouter } from 'next/router'
import { CalendarApi } from '@fullcalendar/common'
import { MdNavigateBefore, MdNavigateNext } from 'react-icons/md'
import { format } from 'date-fns'
import FullCalendar from '@fullcalendar/react'
import { useState } from 'react'

type CalendarToolbarProps = {
  select: {
    id: string
    // start: string
    startStr: string
    // end: string
    endStr: string
    allDay: boolean
  }
  calendarApi: CalendarApi
  calendarId: string | string[]
  calendarName: string
  calendarDate: Date
  calendarRef: FullCalendar
}

const CalendarToolbar = ({
  select,
  calendarApi,
  calendarId,
  calendarName,
  calendarDate,
  calendarRef,
}: CalendarToolbarProps) => {
  const router = useRouter()
  const [currentDate, setCurrentDate] = useState<Date>(calendarApi.getDate())

  calendarApi.on('datesSet', () => setCurrentDate(calendarApi.getDate()))

  return (
    <VStack width="100%" alignItems="flex-end">
      <Flex justify="space-between" width="100%" alignItems="center">
        <Heading size="lg">{format(currentDate, 'MMMM yyyy')}</Heading>
        <Flex grow={2} />
        <Heading size="md">{calendarName}</Heading>
      </Flex>
      <ButtonGroup
        as={Wrap}
        width="100%"
        justify="space-between"
        alignItems="center"
        size="lg"
      >
        <ButtonGroup isAttached variant="outline">
          <IconButton
            aria-label="Navigate Previous Month"
            // margin={2}
            onClick={() => {
              calendarApi.prev()
              calendarRef.render()
            }}
          >
            <MdNavigateBefore />
          </IconButton>
          <Button
            colorScheme="blue"
            // margin={2}
            onClick={() => {
              calendarApi.today()
              calendarRef.render()
            }}
          >
            Today
          </Button>
          <IconButton
            aria-label="Navigate Next Month"
            // margin={2}
            onClick={() => {
              calendarApi.next()
              calendarRef.render()
            }}
          >
            <MdNavigateNext />
          </IconButton>
        </ButtonGroup>

        <Flex grow={2} />
        <Button
          colorScheme="red"
          // margin={2}
          onClick={async () => {
            //@ts-ignore
            removeCalendarEvent(select)

            calendarApi.getEventById(select!.id).remove()
          }}
        >
          Delete Event
        </Button>
        <Button
          colorScheme="green"
          // margin={2}
          onClick={() => {
            router.push({
              pathname: `/calendars/${calendarId}/events`,
              query: {
                selectedStartDate: select.startStr,
                selectedEndDate: select.endStr,
                allDay: select.allDay,
              },
            })
          }}
        >
          Create Event
        </Button>
      </ButtonGroup>
    </VStack>
  )
}

export default CalendarToolbar
