import FullCalendar, { createPlugin, ToolbarInput } from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'
import interactionPlugin from '@fullcalendar/interaction'
import { EventSourceInput } from '@fullcalendar/common'
import { Button, useToast } from '@chakra-ui/react'
import useCalendarEvents from '../../api/useCalendarEvents'
import { useRouter } from 'next/router'
import { useRef, useState } from 'react'
import CalendarToolbar from './CalendarToolbar'

const CustomView = (props) => {
  console.log('CustomView', JSON.stringify(props))
  return <Button>New Event</Button>
}

const customViewPlugin = createPlugin({ views: { custom: CustomView } })

const FullCalendarOverview = () => {
  const router = useRouter()
  const { calendarId } = router.query
  const { calendar, events, updateCalendarEvent, removeCalendarEvent } =
    useCalendarEvents(calendarId)
  const toast = useToast()
  const calendarRef = useRef<FullCalendar | null>(null)
  const [isEventPopoverOpen, setEventPopoverOpen] = useState(false)
  const [select, setSelect] = useState({
    id: undefined,
    startStr: new Date().toISOString().substring(0, 10),
    endStr: undefined,
    allDay: true,
  })
  const [selectedDate, setSelectedDate] = useState({
    dateStr: new Date().toISOString().substring(0, 10),
    allDay: true,
  })

  // console.log('CalendarOverview', select)
  // console.log('calendarRef', calendarRef)

  const eventReceive = (info) => {
    console.log(info.event)
  }

  const eventAdd = (info) => {
    console.log(info.event)
  }

  const eventChange = (info) => {
    console.log('eventChange', info)
    console.log('eventChange', info.event.id)
    console.log('eventChange', JSON.stringify(info.event))
  }

  const eventRemove = (info) => {
    console.log('eventRemove', info)
  }

  const eventDragStop = (info) => {
    console.log(info.event)
  }

  const genericEvent = (info) => {
    console.log('generic', info)
  }

  const onDateClick = (info) => {
    console.log('onDateClick', info)
    setSelectedDate({
      dateStr: info.dateStr,
      allDay: info.allDay,
    })
  }

  const eventClick = (info) => {
    // console.log('eventClick', info)
    console.log(
      'eventClick Obj',
      info.event.toPlainObject({ collapseExtendedProps: true })
    )
    setSelect(info.event.toPlainObject({ collapseExtendedProps: true }))
  }

  const onSelect = (info) => {
    console.log('onSelect', info)
    // setEventPopoverOpen(true)
    setSelect(info)
  }

  const unselect = (info) => {
    console.log('unselect', info)
  }

  // let eventSources = []
  // if (typeof window !== 'undefined') {
  //   eventSources = [
  //     {
  //       url: `https://ticketing.dev/api/calendars/${calendarId}/fullcalendar`,
  //       editable: true,
  //       startEditable: true,
  //       durationEditable: true,
  //       failure: (eventError) => {
  //         toast({ variant: 'error', description: eventError.message })
  //       },
  //     } as EventSourceInput,
  //   ]
  // }

  return (
    <>
      {calendarRef.current && (
        <CalendarToolbar
          calendarId={calendarId}
          select={select}
          calendarApi={calendarRef.current.getApi()}
          calendarRef={calendarRef.current}
          calendarName={calendar.name}
          calendarDate={calendarRef.current.getApi().getDate()}
        />
      )}
      <FullCalendar
        ref={calendarRef}
        plugins={[dayGridPlugin, interactionPlugin, customViewPlugin]}
        // eventSources={eventSources}
        initialView="dayGridMonth"
        nowIndicator={true}
        editable={true}
        selectable={true}
        droppable={true}
        selectMirror={true}
        timeZone="local"
        eventAdd={eventAdd}
        eventChange={updateCalendarEvent}
        eventRemove={eventRemove}
        eventReceive={eventReceive}
        eventDragStop={eventDragStop}
        dateClick={onDateClick}
        eventClick={eventClick}
        select={onSelect}
        unselect={unselect}
        dayMaxEvents={true}
        // dayCellContent={MyDayCellContent}
        events={events}
        headerToolbar={{ right: '', left: '' } as ToolbarInput}
      />
    </>
  )
}

export default FullCalendarOverview
