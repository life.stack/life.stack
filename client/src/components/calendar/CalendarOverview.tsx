import {
  Button,
  ButtonGroup,
  Flex,
  IconButton,
  Spacer,
  useToast,
} from '@chakra-ui/react'
import useCalendarEvents from '../../api/useCalendarEvents'
import { useRouter } from 'next/router'
import React, { useCallback, useMemo, useRef, useState } from 'react'
import {
  Eventcalendar,
  Popup,
  MbscCalendarEvent,
  MbscEventCreatedEvent,
  MbscEventClickEvent,
  MbscEventUpdatedEvent,
  MbscEventDeletedEvent,
  MbscEventcalendarView,
} from '@mobiscroll/react'
import timezonePlugin from './timezonePlugin'
import CreateCalendarEventPopupForm from './events/CreateCalendarEventPopupForm'
import CalendarNavigation from './CalendarNavigation'

const responsivePopup = {
  medium: {
    display: 'anchored',
    width: 550,
    fullScreen: false,
    touchUi: false,
  },
}

const CalendarOverview = () => {
  const router = useRouter()
  const { calendarId } = router.query
  const {
    calendar,
    events,
    updateCalendarEvent,
    removeCalendarEvent,
    createCalendarEvent,
    refreshEvents,
  } = useCalendarEvents(calendarId)
  const toast = useToast()

  const [selectedEvent, setSelectedEvent] = useState<MbscCalendarEvent>(null)
  const [isOpen, setOpen] = useState<boolean>(false)
  const [isEdit, setEdit] = useState<boolean>(false)
  const [anchor, setAnchor] = useState<any>(null)
  const [view, setView] = useState('month')
  const [calView, setCalView] = React.useState<{
    [key: string]: { view: MbscEventcalendarView }
  }>({
    xmsall: {
      view: {
        calendar: { type: 'month' },
      },
    },
    large: {
      view: {
        calendar: { type: 'month' },
        // agenda: { type: 'month' },
      },
    },
  })

  const changeView = (event: any) => {
    console.log(event)
    let view = {}
    switch (event) {
      case 'month':
        view = {
          xmsall: {
            view: {
              calendar: { type: 'month' },
            },
          },
          large: {
            view: {
              calendar: { type: 'month' },
              // agenda: { type: 'month' },
            },
          },
        }
        break
      case 'week':
        view = {
          xsmall: {
            view: {
              calendar: { type: 'week' },
              agenda: { type: 'week' },
            },
          },
        }
        break
      case 'day':
        view = {
          xsmall: {
            view: {
              agenda: { type: 'day' },
            },
          },
          large: {
            view: {
              agenda: { type: 'day' },
              schedule: { type: 'day' },
            },
          },
        }
        break
    }

    setView(event)
    setCalView(view)
  }

  // console.log('CalendarOverview', selectedEvent, isOpen)
  console.log(events)

  const onEventCreated = useCallback<any>(
    (args: MbscEventCreatedEvent) => {
      setSelectedEvent({ ...args.event, title: '' })
      setEdit(false)
      setAnchor(args.target)
      setOpen(true)
    },
    [setSelectedEvent, setEdit, setAnchor, setOpen]
  )

  const onCancel = useCallback(() => {
    setOpen(false)
    refreshEvents()
  }, [setOpen, refreshEvents])

  const onSubmit = useCallback(
    (values) => {
      console.log('Submit Calendar Event', values)
      setOpen(false)
      if (isEdit) {
        updateCalendarEvent(values).then(() => {})
      } else {
        createCalendarEvent(values).then(() => {})
      }
    },
    [setOpen, createCalendarEvent]
  )

  const onEventClick = useCallback(
    (args: MbscEventClickEvent) => {
      setEdit(true)
      setSelectedEvent(args.event)
      setAnchor(args.domEvent.target)
      setOpen(true)
    },
    [setSelectedEvent, setAnchor, setOpen]
  )

  const onEventUpdated = useCallback(
    (args: MbscEventUpdatedEvent) => {
      console.log('onEventUpdated', args.event)
      updateCalendarEvent(args.event).then(() => {})
    },
    [updateCalendarEvent]
  )

  const onEventDeleted = useCallback(
    (args: MbscEventDeletedEvent) => {
      console.log('onEventDeleted', args.event)
      removeCalendarEvent(args.event).then(() => {})
    },
    [removeCalendarEvent]
  )

  const deleteSelectedEvent = useCallback(() => {
    console.log('deleteSelectedEvent', selectedEvent)
    setOpen(false)
    setEdit(false)
    removeCalendarEvent(selectedEvent).then(() => {})
  }, [removeCalendarEvent, selectedEvent, setOpen, setEdit])

  const renderHeader = useCallback(
    () => <CalendarNavigation view={view} changeView={changeView} />,
    [view, changeView]
  )
  return (
    <>
      <Eventcalendar
        data={events}
        dataTimezone="utc"
        displayTimezone="local"
        timezonePlugin={timezonePlugin}
        renderHeader={renderHeader}
        responsive={calView}
        clickToCreate="double"
        dragToCreate={true}
        dragToMove={true}
        dragToResize={true}
        onEventCreated={onEventCreated}
        onEventClick={onEventClick}
        onEventUpdated={onEventUpdated}
        onEventDeleted={onEventDeleted}
      />

      <Popup
        display="bottom"
        // display="center"
        fullScreen={true}
        contentPadding={true}
        // headerText={headerText}
        anchor={anchor}
        // buttons={popupButtons}
        isOpen={isOpen}
        responsive={responsivePopup}
      >
        <CreateCalendarEventPopupForm
          // calendarId={Array.isArray(calendarId) ? calendarId[0] : calendarId}
          selectedEvent={selectedEvent}
          onSubmit={onSubmit}
          onCancel={onCancel}
        />
        {isEdit && (
          <>
            <Button
              mt={4}
              width="100%"
              variant="outline"
              colorScheme="red"
              onClick={deleteSelectedEvent}
            >
              Delete
            </Button>
          </>
        )}
      </Popup>
    </>
  )
}

export default CalendarOverview
