import {
  Collapse,
  forwardRef,
  Heading,
  HStack,
  List,
  ListItem,
  Select,
  Slide,
  Tab,
  TabList,
  TabPanel,
  TabPanels,
  Tabs,
  VStack,
} from '@chakra-ui/react'
import { format, parseISO } from 'date-fns'
import { useState } from 'react'

const ReoccuringEventSelect = forwardRef(({ start, ...props }, ref) => {
  const [selectValue, setSelectValue] = useState(null)
  console.log('ReoccuringEventSelect', props)
  // const startDate = parseISO(start)

  const onSelect = (event) => {
    console.log(event.target.value)
    setSelectValue(event.target.value)
  }

  return (
    <VStack width="100%">
      <Select {...props} ref={ref} onChange={props.onChange}>
        <option value="daily">Daily</option>
        <option value="weekly">Weekly on {format(start, 'EEEE')}</option>
        <option value="monthly">Monthly on the {format(start, 'do')}</option>
        <option value="annually">Annually on {format(start, 'MMMM do')}</option>
        <option value="custom">Custom...</option>
      </Select>
      {/*<Collapse in={selectValue === 'custom'}>*/}
      {/*  <Tabs isFitted variant="enclosed" display="flex">*/}
      {/*    <TabList>*/}
      {/*      <Tab>Daily</Tab>*/}
      {/*      <Tab>Weekly</Tab>*/}
      {/*      <Tab>Monthly</Tab>*/}
      {/*      <Tab>Yearly</Tab>*/}
      {/*    </TabList>*/}
      {/*    <TabPanels>*/}
      {/*      <TabPanel></TabPanel>*/}
      {/*    </TabPanels>*/}
      {/*  </Tabs>*/}
      {/*  <VStack>*/}
      {/*    <Heading size="sm">Frequency</Heading>*/}
      {/*    <HStack>*/}
      {/*      <List>*/}
      {/*        <ListItem>1</ListItem>*/}
      {/*        <ListItem>2</ListItem>*/}
      {/*        <ListItem>3</ListItem>*/}
      {/*        <ListItem>4</ListItem>*/}
      {/*      </List>*/}
      {/*      <List>*/}
      {/*        <ListItem>Day</ListItem>*/}
      {/*        <ListItem>Week</ListItem>*/}
      {/*        <ListItem>Month</ListItem>*/}
      {/*        <ListItem>Year</ListItem>*/}
      {/*      </List>*/}
      {/*    </HStack>*/}
      {/*  </VStack>*/}
      {/*</Collapse>*/}
    </VStack>
  )
})

export default ReoccuringEventSelect
