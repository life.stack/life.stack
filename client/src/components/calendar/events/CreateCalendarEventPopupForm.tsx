import {
  Box,
  Button,
  FormControl,
  FormErrorMessage,
  FormLabel,
  HStack,
  Input,
  VStack,
  Text,
  Heading,
  InputGroup,
  InputLeftAddon,
  FormHelperText,
  Checkbox,
  Tooltip,
  Select,
} from '@chakra-ui/react'
import { SubmitHandler, useForm } from 'react-hook-form'
import { DevTool } from '@hookform/devtools'
import { useRouter } from 'next/router'
import { format, isDate, isValid, parse, parseISO, parseJSON } from 'date-fns'
import { IoLocationSharp } from 'react-icons/io5'
import {
  TiArrowLoop,
  TiMediaPlay,
  TiMediaPlayOutline,
  TiMediaStopOutline,
  TiTime,
} from 'react-icons/ti'
import { Datepicker, MbscCalendarEvent } from '@mobiscroll/react'
import React, {
  MouseEventHandler,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from 'react'

import RrulePicker from '../RrulePicker/RrulePicker'
import timezonePlugin from '../timezonePlugin'
import { DateTime } from 'luxon'

type CreateCalendarEventPopupFormType = {
  selectedEvent: MbscCalendarEvent
  // calendarId: string
  onSubmit: SubmitHandler<MbscCalendarEvent>
  onCancel: MouseEventHandler<HTMLButtonElement>
}

const getDefaultDate = (eventDate: Date | string | {}): string => {
  console.log('getDefaultDate', typeof eventDate, eventDate)
  if (typeof eventDate === 'string') {
    return eventDate as string
  } else {
    return parseJSON(JSON.stringify(eventDate)).toISOString()
  }
}

const CreateCalendarEventPopupForm = ({
  selectedEvent,
  onSubmit,
  onCancel,
}: CreateCalendarEventPopupFormType) => {
  const [start, startRef] = useState<any>(null)
  const [end, endRef] = useState<any>(null)
  console.log(selectedEvent)
  const { register, control, handleSubmit, getValues, setValue, watch } =
    useForm({
      defaultValues: {
        id: selectedEvent.id ? selectedEvent.id : null,
        title: selectedEvent.title,
        location: selectedEvent.location ? selectedEvent.location : '',
        start: selectedEvent.start,
        end: selectedEvent.end,
        allDay: selectedEvent.allDay,
        recurring: selectedEvent.recurring ? selectedEvent.recurring : '',
      },
    })

  useEffect(() => {
    register('recurring')
  }, [register])

  const startString = getValues('start')
  //@ts-ignore
  let startDate = parseJSON(startString)
  if (!isDate(startString) || !isValid(startDate)) {
    // @ts-ignore
    startDate = parse(startString, 'MM/dd/yyyy', new Date())
    if (!isValid(startDate) && typeof startString !== 'object') {
      // @ts-ignore
      startDate = DateTime.fromISO(startString).toJSDate()
    }
  }
  console.log(startDate, getValues('start'))

  const controls = useMemo<any>(
    () => (getValues('allDay') ? ['date'] : ['datetime']),
    [watch('allDay')]
  )

  const respSetting = useMemo<any>(
    () =>
      watch('allDay')
        ? {
            medium: {
              controls: ['calendar'],
              touchUi: false,
            },
          }
        : {
            medium: {
              controls: ['calendar', 'time'],
              touchUi: false,
            },
          },
    [getValues('allDay')]
  )

  const onDatechange = useCallback(
    (args) => {
      console.log('onDatechange', args)

      setValue('start', args.value[0])
      setValue('end', args.value[1])
    },
    [setValue]
  )

  const onRecurringChange = useCallback(
    (recurringValue) => {
      console.log('onRecurringChange', recurringValue)
      setValue('recurring', recurringValue)
    },
    [setValue]
  )

  return (
    <VStack
      as="form"
      onSubmit={handleSubmit(onSubmit)}
      aria-label="Add an event to the calendar"
    >
      <HStack justify="space-between" width="100%">
        <Button onClick={onCancel} variant="outline">
          Cancel
        </Button>
        <Heading as="h4" size="md">
          New Event
        </Heading>
        <Button type="submit" colorScheme="green">
          Save
        </Button>
      </HStack>

      <Input
        {...register('title')}
        id="title"
        aria-label="title"
        placeholder="Enter title"
      />
      <InputGroup>
        <InputLeftAddon>
          <IoLocationSharp />
        </InputLeftAddon>
        <Input
          {...register('location')}
          id="location"
          aria-label="location"
          placeholder="Add location"
        />
      </InputGroup>
      <InputGroup>
        <InputLeftAddon>
          <TiTime />
        </InputLeftAddon>
        <Checkbox
          {...register('allDay')}
          id="allDay"
          aria-label="allDay"
          placeholder="allDay"
          size="lg"
          marginLeft="3"
        >
          All Day
        </Checkbox>
      </InputGroup>
      <InputGroup>
        <InputLeftAddon>
          <TiMediaPlayOutline />
        </InputLeftAddon>
        <Input
          {...register('start')}
          ref={startRef}
          id="start"
          aria-label="start time"
          placeholder="Start time"
          // type={watch('allDay') ? 'date' : 'datetime-local'}
        />
      </InputGroup>
      <Tooltip label="This day is exclusive">
        <InputGroup>
          <InputLeftAddon>
            <TiMediaStopOutline />
          </InputLeftAddon>
          <Input
            {...register('end')}
            ref={endRef}
            id="end"
            aria-label="end time"
            placeholder="End time"
            // type={watch('allDay') ? 'date' : 'datetime-local'}
          />
        </InputGroup>
      </Tooltip>
      <Datepicker
        dataTimezone="utc"
        displayTimezone="local"
        timezonePlugin={timezonePlugin}
        select="range"
        controls={controls}
        touchUi={true}
        startInput={start}
        endInput={end}
        showRangeLabels={false}
        responsive={respSetting}
        onChange={onDatechange}
        value={watch(['start', 'end'])}

        // returnFormat="iso8601"
      />

      <RrulePicker
        startDate={startDate}
        onRecurringChange={onRecurringChange}
        initialRruleString={getValues('recurring')}
      />

      <DevTool control={control} />
    </VStack>
  )
}

export default CreateCalendarEventPopupForm
