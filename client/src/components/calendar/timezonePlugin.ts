import { luxonTimezone as timezonePlugin } from '@mobiscroll/react'
import * as luxon from 'luxon'
timezonePlugin.luxon = luxon

export default timezonePlugin
