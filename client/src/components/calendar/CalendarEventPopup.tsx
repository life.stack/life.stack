import React, { useCallback, useMemo, useState } from 'react'
import {
  Button,
  Datepicker,
  Input,
  Popup,
  SegmentedGroup,
  SegmentedItem,
  Switch,
  Textarea,
} from '@mobiscroll/react'
import useCalendarEvent from '../../api/useCalendarEvent'
import CreateCalendarEventForm from './events/CreateCalendarEventForm'
import CreateCalendarEventPopupForm from './events/CreateCalendarEventPopupForm'

const responsivePopup = {
  medium: {
    display: 'anchored',
    width: 550,
    fullScreen: false,
    touchUi: false,
  },
}

const CalendarEventPopup = ({
  calendarId,
  saveEvent,
  isEdit,
  isOpen,
  setOpen,
  anchor,
  popupEventDate,
}) => {
  // console.log('CalendarEventPopup', popupEventDate)
  // const headerText = useMemo<string>(
  //   () => (isEdit ? 'Edit event' : 'New Event'),
  //   [isEdit]
  // )
  // const popupButtons = useMemo<any>(() => {
  //   if (isEdit) {
  //     return [
  //       'cancel',
  //       {
  //         handler: () => {
  //           saveEvent()
  //         },
  //         keyCode: 'enter',
  //         text: 'Save',
  //         cssClass: 'mbsc-popup-button-primary',
  //       },
  //     ]
  //   } else {
  //     return [
  //       'cancel',
  //       {
  //         handler: () => {
  //           saveEvent()
  //         },
  //         keyCode: 'enter',
  //         text: 'Add',
  //         cssClass: 'mbsc-popup-button-primary',
  //       },
  //     ]
  //   }
  // }, [isEdit, saveEvent])

  const onCancel = () => {
    setOpen(false)
  }

  return (
    <Popup
      display="bottom"
      fullScreen={true}
      contentPadding={true}
      // headerText={headerText}
      anchor={anchor}
      // buttons={popupButtons}
      isOpen={isOpen}
      responsive={responsivePopup}
    >
      <CreateCalendarEventPopupForm
        calendarId={calendarId}
        selectedStartDate={popupEventDate[0]}
        selectedEndDate={popupEventDate[1]}
        onCancel={onCancel}
      />
    </Popup>
  )
}

export default CalendarEventPopup
