import React, { useCallback, useMemo, useState } from 'react'
import {
  Button,
  Datepicker,
  Input,
  Popup,
  SegmentedGroup,
  SegmentedItem,
  Switch,
  Textarea,
} from '@mobiscroll/react'
import useCalendarEvent from '../../api/useCalendarEvent'

const responsivePopup = {
  medium: {
    display: 'anchored',
    width: 400,
    fullScreen: false,
    touchUi: false,
  },
}

const MobiCalendarEventPopup = ({
  calendarId,
  saveEvent,
  isEdit,
  isOpen,
  setOpen,
  popupEventAllDay,
  anchor,
  setTitle,
  setDescription,
  setAllDay,
  setDate,
  setStatus,
  deleteEvent,
  popupEventDescription,
  popupEventStatus,
  tempEvent,
  popupEventDate,
  popupEventTitle,
  events,
  setMyEvents,
}) => {
  const { createCalendarEvent } = useCalendarEvent(calendarId)
  const [start, startRef] = useState<any>(null)
  const [end, endRef] = useState<any>(null)

  // handle popup form changes

  const titleChange = useCallback<any>((ev: any) => {
    setTitle(ev.target.value)
  }, [])

  const descriptionChange = useCallback<any>((ev: any) => {
    setDescription(ev.target.value)
  }, [])

  const allDayChange = useCallback<any>((ev: any) => {
    setAllDay(ev.target.checked)
  }, [])

  const dateChange = useCallback<any>((args: any) => {
    setDate(args.value)
  }, [])

  const statusChange = useCallback<any>((ev: any) => {
    setStatus(ev.target.value)
  }, [])

  const onDeleteClick = useCallback<any>(() => {
    deleteEvent(tempEvent)
    setOpen(false)
  }, [deleteEvent, tempEvent])

  // datepicker options
  const controls = useMemo<any>(
    () => (popupEventAllDay ? ['date'] : ['datetime']),
    [popupEventAllDay]
  )
  const respSetting = useMemo<any>(
    () =>
      popupEventAllDay
        ? {
            medium: {
              controls: ['calendar'],
              touchUi: false,
            },
          }
        : {
            medium: {
              controls: ['calendar', 'time'],
              touchUi: false,
            },
          },
    [popupEventAllDay]
  )

  const headerText = useMemo<string>(
    () => (isEdit ? 'Edit event' : 'New Event'),
    [isEdit]
  )
  const popupButtons = useMemo<any>(() => {
    if (isEdit) {
      return [
        'cancel',
        {
          handler: () => {
            saveEvent()
          },
          keyCode: 'enter',
          text: 'Save',
          cssClass: 'mbsc-popup-button-primary',
        },
      ]
    } else {
      return [
        'cancel',
        {
          handler: () => {
            saveEvent()
          },
          keyCode: 'enter',
          text: 'Add',
          cssClass: 'mbsc-popup-button-primary',
        },
      ]
    }
  }, [isEdit, saveEvent])

  const onClose = useCallback(() => {
    if (!isEdit) {
      // refresh the list, if add popup was canceled, to remove the temporary event
      setMyEvents([...events])
    }
    setOpen(false)
  }, [isEdit, events])

  return (
    <Popup
      display="bottom"
      fullScreen={true}
      contentPadding={false}
      headerText={headerText}
      anchor={anchor}
      buttons={popupButtons}
      isOpen={isOpen}
      onClose={onClose}
      responsive={responsivePopup}
    >
      <div className="mbsc-form-group">
        <Input label="Title" value={popupEventTitle} onChange={titleChange} />
        <Textarea
          label="Description"
          value={popupEventDescription}
          onChange={descriptionChange}
        />
      </div>
      <div className="mbsc-form-group">
        <Switch
          label="All-day"
          checked={popupEventAllDay}
          onChange={allDayChange}
        />
        <Input ref={startRef} label="Starts" />
        <Input ref={endRef} label="Ends" />
        <Datepicker
          select="range"
          controls={controls}
          touchUi={true}
          startInput={start}
          endInput={end}
          showRangeLabels={false}
          responsive={respSetting}
          onChange={dateChange}
          value={popupEventDate}
        />
        <SegmentedGroup onChange={statusChange}>
          <SegmentedItem value="busy" checked={popupEventStatus === 'busy'}>
            Show as busy
          </SegmentedItem>
          <SegmentedItem value="free" checked={popupEventStatus === 'free'}>
            Show as free
          </SegmentedItem>
        </SegmentedGroup>
        {isEdit ? (
          <div className="mbsc-button-group">
            <Button
              className="mbsc-button-block"
              color="danger"
              variant="outline"
              onClick={onDeleteClick}
            >
              Delete event
            </Button>
          </div>
        ) : null}
      </div>
    </Popup>
  )
}

export default MobiCalendarEventPopup
