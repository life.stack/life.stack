import { DayCellContentArg } from '@fullcalendar/common'
import {
  ButtonGroup,
  HStack,
  IconButton,
  VStack,
  Text,
  Box,
  Flex,
} from '@chakra-ui/react'
import { IoMdConstruct } from 'react-icons/io'
import { MdDelete } from 'react-icons/md'

const MyDayCellContent = (props: DayCellContentArg) => {
  if (props.isToday) {
    console.log('MyDayCellContent', JSON.stringify(props))
  }

  return (
    <Flex width="100%" direction="row" justify="space-between">
      <ButtonGroup size="xs">
        <IconButton aria-label="add event">
          <IoMdConstruct />
        </IconButton>
        <IconButton aria-label="delete event">
          <MdDelete />
        </IconButton>
      </ButtonGroup>
      <Text fontSize="lg">{props.dayNumberText}</Text>
    </Flex>
  )
}

export default MyDayCellContent
