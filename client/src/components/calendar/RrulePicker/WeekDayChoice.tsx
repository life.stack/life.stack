import { HStack, Select } from '@chakra-ui/react'
import weekdays from './weekdays'

const WeekDayChoice = ({
  setByWeekdays,
  byWeekdays,
  bySetPos,
  setBySetPos,
}) => {
  const handlePosChange = (event) => {
    setBySetPos(event.target.value)
  }

  const handleWeekdayChange = (event) => {
    setByWeekdays([event.target.value])
  }
  console.log('WeekDayChoice', bySetPos, byWeekdays)

  return (
    <>
      <HStack>
        <Select onChange={handlePosChange} value={bySetPos}>
          <option value="1">First</option>
          <option value="2">Second</option>
          <option value="3">Third</option>
          <option value="4">Fourth</option>
          <option value="5">Fifth</option>
          <option value="-1">Last</option>
        </Select>
        <Select onChange={handleWeekdayChange} value={byWeekdays[0]}>
          {weekdays.map((day) => {
            return (
              <option key={day.value} value={day.value}>
                {day.name}
              </option>
            )
          })}
        </Select>
      </HStack>
    </>
  )
}

export default WeekDayChoice
