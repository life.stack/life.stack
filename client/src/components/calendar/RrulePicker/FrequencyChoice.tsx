import { Box, HStack, useRadio, useRadioGroup } from '@chakra-ui/react'
import { frequencyOptions } from './frequency'

const FrequencyChoice = ({ setFrequency, frequency }) => {
  // console.log('FrequencyChoice', typeof frequency, frequency)
  const { getRootProps, getRadioProps } = useRadioGroup({
    name: 'frequency',
    onChange: setFrequency,
    value: Number(frequency),
  })

  const group = getRootProps()

  return (
    <HStack {...group} marginBottom={2}>
      {frequencyOptions.map(({ text, value }) => {
        const radio = getRadioProps({ value })
        return (
          <FrequencyCard key={value} {...radio}>
            {text}
          </FrequencyCard>
        )
      })}
    </HStack>
  )
}

const FrequencyCard = (props) => {
  const { getInputProps, getCheckboxProps } = useRadio(props)

  const input = getInputProps()
  const checkbox = getCheckboxProps()

  return (
    <Box as="label">
      <input {...input} />
      <Box
        {...checkbox}
        cursor="pointer"
        borderWidth="1px"
        borderRadius="md"
        boxShadow="md"
        _checked={{
          bg: 'teal.600',
          color: 'white',
          borderColor: 'teal.600',
        }}
        _focus={{
          boxShadow: 'outline',
        }}
        px={5}
        py={3}
      >
        {props.children}
      </Box>
    </Box>
  )
}

export default FrequencyChoice
