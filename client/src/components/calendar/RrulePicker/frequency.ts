import { Frequency } from 'rrule'

export const frequencyOptions = [
  { text: 'Daily', value: Frequency.DAILY },
  { text: 'Weekly', value: Frequency.WEEKLY },
  { text: 'Monthly', value: Frequency.MONTHLY },
  { text: 'Yearly', value: Frequency.YEARLY },
]
