import { CheckboxGroup, Checkbox, Wrap } from '@chakra-ui/react'
import monthdays from './monthdays'

const MonthDayChoice = ({ setByMonthday, byMonthday }) => {
  return (
    <CheckboxGroup onChange={setByMonthday} size="lg" value={byMonthday}>
      <Wrap shouldWrapChildren spacing={3}>
        {monthdays.map((day) => (
          <Checkbox key={day} value={day}>
            {day}
          </Checkbox>
        ))}
      </Wrap>
    </CheckboxGroup>
  )
}

export default MonthDayChoice
