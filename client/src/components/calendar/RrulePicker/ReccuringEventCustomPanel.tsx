import {
  Box,
  HStack,
  Text,
  InputGroup,
  InputLeftAddon,
  InputRightAddon,
  NumberDecrementStepper,
  NumberIncrementStepper,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  useRadio,
  useRadioGroup,
  Select,
  VStack,
} from '@chakra-ui/react'

import FrequencyChoice from './FrequencyChoice'
import MonthChoice from './MonthChoice'
import { useEffect, useState } from 'react'
import WeekDayChoice from './WeekDayChoice'
import WeekDaysChoice from './WeekDaysChoice'
import MonthDayChoice from './MonthDayChoice'
import { Frequency } from 'rrule'

const ReccuringEventSelectCustomPanel = ({
  frequency,
  setFrequency,
  interval,
  setInterval,
  byMonth,
  setByMonth,
  bySetPos,
  setBySetPos,
  byWeekdays,
  setByWeekdays,
  byMonthday,
  setByMonthday,
}) => {
  const [monthSelect, setMonthSelect] = useState<string | 'each' | 'onThe'>(
    bySetPos ? 'onThe' : 'each'
  )

  return (
    <Box>
      <FrequencyChoice setFrequency={setFrequency} frequency={frequency} />
      <InputGroup>
        <InputLeftAddon>Repeat Every</InputLeftAddon>
        <NumberInput
          defaultValue={1}
          min={1}
          value={interval}
          onChange={setInterval}
          allowMouseWheel
        >
          <NumberInputField />
          <NumberInputStepper>
            <NumberIncrementStepper />
            <NumberDecrementStepper />
          </NumberInputStepper>
        </NumberInput>
        {frequency == Frequency.DAILY && (
          <InputRightAddon>{interval === 1 ? 'day' : 'days'}</InputRightAddon>
        )}
        {frequency == Frequency.WEEKLY && (
          <InputRightAddon>weeks</InputRightAddon>
        )}
        {frequency == Frequency.MONTHLY && (
          <InputRightAddon>months on day</InputRightAddon>
        )}
        {frequency == Frequency.YEARLY && (
          <InputRightAddon>
            {interval === 1 ? 'year' : 'years'} on
          </InputRightAddon>
        )}
      </InputGroup>
      {frequency == Frequency.WEEKLY && (
        <>
          <WeekDaysChoice
            setByWeekdays={setByWeekdays}
            byWeekdays={byWeekdays}
          />
        </>
      )}
      {frequency == Frequency.MONTHLY && (
        <>
          <Select
            value={monthSelect}
            onChange={(event) => {
              setMonthSelect(event.target.value)
            }}
          >
            <option value="each">Each</option>
            <option value="onThe">On the...</option>
          </Select>
          {monthSelect === 'each' && (
            <MonthDayChoice
              setByMonthday={setByMonthday}
              byMonthday={byMonthday}
            />
          )}
          {monthSelect === 'onThe' && (
            <WeekDayChoice
              setByWeekdays={setByWeekdays}
              byWeekdays={byWeekdays}
              bySetPos={bySetPos}
              setBySetPos={setBySetPos}
            />
          )}
        </>
      )}
      {frequency == Frequency.YEARLY && (
        <>
          <MonthChoice setMonths={setByMonth} byMonth={byMonth} />
          <WeekDayChoice
            setByWeekdays={setByWeekdays}
            byWeekdays={byWeekdays}
            bySetPos={bySetPos}
            setBySetPos={setBySetPos}
          />
        </>
      )}
    </Box>
  )
}

export default ReccuringEventSelectCustomPanel
