import { Frequency, RRule } from 'rrule'
import pickBy from 'lodash/pickBy'
import identity from 'lodash/identity'
import { daysToEnum } from './weekdays'
export type FrequencyType = 'daily' | 'weekly' | 'monthly' | 'yearly'
// type useRecurringRuleType = function({select: string, {
//
// }})

const generateRecurringRule = (
  select,
  {
    frequency,
    count,
    interval,
    byMonth,
    // byWeekday,
    byWeekdays,
    bySetPos,
    byMonthday,
  }
) => {
  // console.log('useRecurringRule', select, {
  //   frequency,
  //   count,
  //   interval,
  //   byMonth,
  //   // byWeekday,
  //   byWeekdays,
  //   bySetPos,
  //   byMonthday,
  // })
  const isCustom = ['custom', 'custom-value'].includes(select)

  let recurringString = ''
  let rrule = undefined

  if (!isCustom) {
    try {
      rrule = RRule.fromText(`every ${select}`)
    } catch (e) {
      // console.error('rrule', e)
    }
  } else if (!rrule && ['custom', 'custom-value'].includes(select)) {
    const rruleParams = pickBy(
      {
        freq: frequency,
        count,
        interval,
        bymonth: byMonth.length > 0 ? byMonth : undefined,
        byweekday: validateByWeekday({ byWeekdays }),
        bysetpos: bySetPosIsValid(bySetPos) ? Number(bySetPos) : undefined,
        bymonthday: validateByMonthday({ byMonthday }),
      },
      (v, k) => {
        return k === 'freq' || ![undefined, null, ''].includes(v)
      }
    )
    // console.log('rruleParams', rruleParams)
    rrule = new RRule(rruleParams)
  }

  // console.log(rrule)
  //
  // if (rrule) {
  //   recurringString = rrule.toString()
  // }
  //
  // console.log(recurringString)
  return rrule
}

const validateByMonthday = ({ byMonthday }) => {
  return byMonthday.length > 0 ? byMonthday : undefined
}

const validateByWeekday = ({ byWeekdays }) => {
  return byWeekdays.length > 0 ? byWeekdays.map(daysToEnum) : undefined
}

const bySetPosIsValid = (value) => {
  let valid = false
  try {
    const valueNum = Number(value)
    if (valueNum >= -366 && valueNum <= 366 && valueNum !== 0) {
      valid = true
    }
  } catch (e) {}
  return valid
}

const frequencyToEnum = (frequency: FrequencyType) => {
  switch (frequency) {
    case 'daily':
      return Frequency.DAILY
    case 'weekly':
      return Frequency.WEEKLY
    case 'monthly':
      return Frequency.MONTHLY
    case 'yearly':
      return Frequency.YEARLY
  }
}

export default generateRecurringRule
