import {
  Box,
  Checkbox,
  CheckboxGroup,
  Flex,
  HStack,
  useCheckbox,
  useCheckboxGroup,
  Wrap,
} from '@chakra-ui/react'
import monthValues from './months'

const MonthChoice = ({ setMonths, byMonth }) => {
  return (
    <CheckboxGroup size="lg" onChange={setMonths}>
      <Wrap spacing={2}>
        {monthValues.map(({ text, value }) => {
          return (
            <Checkbox
              key={value}
              value={value}
              isChecked={byMonth.includes(value)}
            >
              {text}
            </Checkbox>
          )
        })}
      </Wrap>
    </CheckboxGroup>
  )
}

const MonthCard = (props) => {
  const { getInputProps, getCheckboxProps } = useCheckbox(props)

  const input = getInputProps()
  const checkbox = getCheckboxProps()

  return (
    <Box as="label">
      <input {...input} />
      <Box
        {...checkbox}
        cursor="pointer"
        borderWidth="1px"
        borderRadius="md"
        boxShadow="md"
        _checked={{
          bg: 'teal.600',
          color: 'white',
          borderColor: 'teal.600',
        }}
        _focus={{
          boxShadow: 'outline',
        }}
        px={5}
        py={3}
      >
        {props.children}
      </Box>
    </Box>
  )
}

export default MonthChoice
