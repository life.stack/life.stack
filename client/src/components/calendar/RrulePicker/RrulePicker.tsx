import {
  Box,
  forwardRef,
  InputGroup,
  InputLeftAddon,
  Select,
} from '@chakra-ui/react'
import { TiArrowLoop } from 'react-icons/ti'
import { format } from 'date-fns'
import React, { useEffect, useState } from 'react'
import generateRecurringRule from './generateRecurringRule'
import ReccuringEventSelectCustomPanel from './ReccuringEventCustomPanel'
import RRule, { Frequency } from 'rrule'
import castArray from 'lodash/castArray'

const RrulePicker = forwardRef(
  ({ startDate, onRecurringChange, initialRruleString }, ref) => {
    // console.log('RrulePicker')
    let initialRuleOptions = null
    if (
      typeof initialRruleString === 'string' &&
      initialRruleString.length > 0
    ) {
      const rrule = RRule.fromString(initialRruleString)
      console.log('initialOptions', initialRruleString, rrule)
      initialRuleOptions = rrule.origOptions
      initialRuleOptions.ruleText = rrule.toText()
    }

    const [select, setSelect] = useState(initialRuleOptions ? 'custom' : '')
    const [rruleText, setRruleText] = useState(
      initialRuleOptions ? initialRuleOptions.ruleText : ''
    )
    const [frequency, setFrequency] = useState(
      initialRuleOptions ? initialRuleOptions.freq : Frequency.DAILY
    )

    const [count, setCount] = useState(
      initialRuleOptions && initialRuleOptions.count
        ? initialRuleOptions.count
        : ''
    )
    const [interval, setInterval] = useState(
      initialRuleOptions ? initialRuleOptions.interval : 1
    )
    const [byMonth, setByMonth] = useState(
      initialRuleOptions && initialRuleOptions.bymonth
        ? castArray(initialRuleOptions.bymonth)
        : []
    )
    // const [byWeekday, setByWeekday] = useState(
    //   initialRuleOptions && initialRuleOptions.byweekday
    //     ? initialRuleOptions.byweekday[0].weekday
    //     : []
    // )
    const [byWeekdays, setByWeekdays] = useState(
      initialRuleOptions && initialRuleOptions.byweekday
        ? initialRuleOptions.byweekday.map((w) => `${w.weekday}`)
        : []
    )
    const [bySetPos, setBySetPos] = useState(
      initialRuleOptions && initialRuleOptions.bysetpos
        ? initialRuleOptions.bysetpos
        : []
    )
    const [byMonthday, setByMonthday] = useState(
      initialRuleOptions && initialRuleOptions.bymonthday
        ? initialRuleOptions.bymonthday.map((m) => m.valueOf())
        : []
    )

    // console.log('RrulePicker', {
    //   frequency,
    //   count,
    //   interval,
    //   byMonth,
    //   // byWeekday,
    //   byWeekdays,
    //   bySetPos,
    //   byMonthday,
    // })

    const onSelect = (event) => {
      // console.log('Select Change: ', event.target.value)
      // onChange(event)
      setSelect(event.target.value)
    }

    useEffect(() => {
      const rrule = generateRecurringRule(select, {
        frequency,
        count,
        interval,
        byMonth,
        // byWeekday,
        byWeekdays,
        bySetPos,
        byMonthday,
      })
      if (rrule) {
        // console.log(rrule.toString())
        // console.log(rrule.toText())
        setRruleText(rrule.toText())
        onRecurringChange(rrule.toString().replace('RRULE:', ''))
        // onChange(rrule.toString().replace('RRULE:', ''))
      } else {
        // console.log('rrule not valid')
      }
    }, [
      select,
      frequency,
      count,
      interval,
      byMonth,
      // byWeekday,
      byWeekdays,
      bySetPos,
      byMonthday,
      onRecurringChange,
    ])

    return (
      <>
        <InputGroup>
          <InputLeftAddon>
            <TiArrowLoop />
          </InputLeftAddon>

          <Select onChange={onSelect} value={select} ref={ref}>
            <option value="">one time event</option>
            <option value="day">Daily</option>
            <option value="week">Weekly on {format(startDate, 'EEEE')}</option>
            <option value="month">
              Monthly on the {format(startDate, 'do')}
            </option>
            <option value="year">
              Annually on {format(startDate, 'MMMM do')}
            </option>
            <option value="custom">
              {select === 'custom' && rruleText.length > 0
                ? rruleText
                : 'Custom...'}
            </option>
          </Select>
        </InputGroup>

        {select === 'custom' && (
          <ReccuringEventSelectCustomPanel
            frequency={frequency}
            setFrequency={setFrequency}
            interval={interval}
            setInterval={setInterval}
            byMonth={byMonth}
            setByMonth={setByMonth}
            // byWeekday={byWeekday}
            // setByWeekday={setByWeekday}
            bySetPos={bySetPos}
            setBySetPos={setBySetPos}
            byWeekdays={byWeekdays}
            setByWeekdays={setByWeekdays}
            byMonthday={byMonthday}
            setByMonthday={setByMonthday}
          />
        )}
      </>
    )
  }
)

export default RrulePicker
