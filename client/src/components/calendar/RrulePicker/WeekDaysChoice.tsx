import { CheckboxGroup, Checkbox } from '@chakra-ui/react'
import weekdays from './weekdays'
import { toNumber } from 'lodash'

const WeekDaysChoice = ({ setByWeekdays, byWeekdays }) => {
  // console.log('WeekDaysChoice', typeof byWeekdays, byWeekdays)
  return (
    <CheckboxGroup onChange={setByWeekdays} defaultValue={byWeekdays}>
      {weekdays.map((day) => {
        // console.log(day)
        return (
          <Checkbox key={day.value} value={day.value}>
            {day.name}
          </Checkbox>
        )
      })}
    </CheckboxGroup>
  )
}

export default WeekDaysChoice
