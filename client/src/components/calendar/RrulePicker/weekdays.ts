import RRule, { Weekday } from 'rrule'
import find from 'lodash/find'

const weekdays = [
  {
    name: 'Sunday',
    value: `${RRule.SU.weekday}`,
    rrule: RRule.SU,
  },
  {
    name: 'Monday',
    value: `${RRule.MO.weekday}`,
    rrule: RRule.MO,
  },
  {
    name: 'Tuesday',
    value: `${RRule.TU.weekday}`,
    rrule: RRule.TU,
  },
  {
    name: 'Wednesday',
    value: `${RRule.WE.weekday}`,
    rrule: RRule.WE,
  },
  {
    name: 'Thursday',
    value: `${RRule.TH.weekday}`,
    rrule: RRule.TH,
  },
  {
    name: 'Friday',
    value: `${RRule.FR.weekday}`,
    rrule: RRule.FR,
  },
  {
    name: 'Saturday',
    value: `${RRule.SA.weekday}`,
    rrule: RRule.SA,
  },
]

export const daysToEnum = (day) => {
  const theDay = find(weekdays, (d) => d.value === day)
  return theDay.rrule
}

export default weekdays
