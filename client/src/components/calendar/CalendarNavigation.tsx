import {
  Box,
  ButtonGroup,
  Checkbox,
  Flex,
  HStack,
  IconButton,
  Spacer,
  useRadio,
  useRadioGroup,
} from '@chakra-ui/react'
import {
  CalendarNav,
  CalendarNext,
  CalendarPrev,
  CalendarToday,
  SegmentedGroup,
  SegmentedItem,
} from '@mobiscroll/react'
import { BsCalendar, BsCalendarDay, BsCalendarWeek } from 'react-icons/bs'
import React from 'react'

const CalendarNavigation = ({ view, changeView }) => {
  const { getRootProps, getRadioProps } = useRadioGroup({
    name: 'view',
    onChange: changeView,
    value: view,
  })

  const group = getRootProps()

  return (
    <Flex width="100%" align="center">
      <CalendarNav />

      <Spacer />

      <ButtonGroup isAttached variant="ghost" size="lg">
        <IconButton
          aria-label="Month view"
          icon={<BsCalendar size={32} />}
          isActive={view === 'month'}
          onClick={() => changeView('month')}
        />
        <IconButton
          aria-label="Week view"
          icon={<BsCalendarWeek size={32} />}
          isActive={view === 'week'}
          onClick={() => changeView('week')}
        />
        <IconButton
          aria-label="Day view"
          icon={<BsCalendarDay size={32} />}
          isActive={view === 'day'}
          onClick={() => changeView('day')}
        />
      </ButtonGroup>

      <Spacer />
      <Flex>
        <CalendarPrev />
        <CalendarToday />
        <CalendarNext />
      </Flex>
    </Flex>
  )
}

export default CalendarNavigation
