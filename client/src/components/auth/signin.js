import { Heading, Link, Stack, Text } from '@chakra-ui/react'
import AuthForm from './AuthForm'
import { useUsers } from '../../api/useUsers'

const Signin = () => {
  const { signin } = useUsers()

  return (
    <AuthForm onSubmit={signin}>
      <Stack align="center">
        <Heading fontSize="4xl">Sign in to your account</Heading>
        <Text fontSize="lg" color="gray.600">
          to enjoy all of our cool <Link color="blue.400">features</Link> ✌️
        </Text>
      </Stack>
    </AuthForm>
  )
}

export default Signin
