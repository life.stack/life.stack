import {
  Box,
  Button,
  Checkbox,
  Flex,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Input,
  Link,
  Stack,
  Text,
  useColorModeValue,
} from '@chakra-ui/react'
import { FieldValues, SubmitHandler, useForm } from 'react-hook-form'
import { ReactElement } from 'react'

type AuthFormProps = {
  signup?: Boolean
  children: ReactElement
  onSubmit: SubmitHandler<FieldValues>
  errors?: object
}

function AuthForm({
  signup,
  onSubmit,
  children,
  errors,
}: AuthFormProps): ReactElement {
  const { register, handleSubmit, watch, formState } = useForm()

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Flex
          align="center"
          justify="center"
          bg={useColorModeValue('gray.50', 'gray.800')}
        >
          <Stack spacing={8} mx="auto" maxW="lg" px={6}>
            {children}
            <Box
              rounded="lg"
              bg={useColorModeValue('white', 'gray.700')}
              boxShadow="lg"
              p={8}
            >
              <Stack spacing={4}>
                {signup && (
                  <FormControl id="displayName">
                    <FormLabel>Name</FormLabel>
                    <Input
                      {...register('displayName')}
                      placeholder="What should we call you?"
                    />
                    <FormErrorMessage>
                      {formState.errors.displayName}
                    </FormErrorMessage>
                  </FormControl>
                )}
                <FormControl id="email">
                  <FormLabel>Email address</FormLabel>
                  <Input type="email" {...register('email')} />
                  <FormErrorMessage>{formState.errors.email}</FormErrorMessage>
                </FormControl>
                <FormControl id="password">
                  <FormLabel>Password</FormLabel>
                  <Input type="password" {...register('password')} />
                  <FormErrorMessage>
                    {formState.errors.password}
                  </FormErrorMessage>
                </FormControl>
                <Stack spacing={10}>
                  <Stack
                    direction={{ base: 'column', sm: 'row' }}
                    align="start"
                    justify="space-between"
                  >
                    <Checkbox>Remember me</Checkbox>
                    <Link color="blue.400">Forgot password?</Link>
                  </Stack>
                  <Button
                    type="submit"
                    bg="blue.400"
                    color="white"
                    _hover={{
                      bg: 'blue.500',
                    }}
                  >
                    {signup ? 'Sign up' : 'Sign in'}
                  </Button>
                  {!signup && (
                    <Text align="center" fontSize="sm" color="gray.600">
                      <Link color="blue.400" href="/auth/signup">
                        or sign up for an account now!
                      </Link>
                    </Text>
                  )}
                </Stack>
              </Stack>
            </Box>
          </Stack>
        </Flex>
      </form>
    </>
  )
}

export default AuthForm
