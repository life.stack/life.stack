import {
  Avatar,
  Flex,
  FormControl,
  FormLabel,
  useBreakpointValue,
  useColorModeValue,
} from '@chakra-ui/react'
import { useState } from 'react'
import AvatarEditor from 'react-avatar-edit'

const UserAvatarForm = ({ onAvatarChange }) => {
  const [preview, setPreview] = useState(null)
  const avatarEditorWidth = useBreakpointValue({ base: 168, md: 240 })
  const previewWidth = useBreakpointValue({ base: 20, md: 24 })
  const avtarBg = useColorModeValue('gray.100', 'gray.800')

  // console.log('Editor Width', avatarEditorWidth)

  function onClose() {
    setPreview(null)
  }
  function onCrop(avatarPreview) {
    setPreview(avatarPreview)
    onAvatarChange(avatarPreview)
  }

  return (
    <FormControl>
      <FormLabel
        fontSize="sm"
        fontWeight="md"
        color={useColorModeValue('gray.700', 'gray.50')}
      >
        Avatar
      </FormLabel>
      <Flex
        alignItems="center"
        justifyContent="center"
        direction="column"
        mt={1}
      >
        <AvatarEditor
          width={avatarEditorWidth}
          height={avatarEditorWidth}
          onCrop={onCrop}
          onClose={onClose}
          exportSize={128}
        />
        {preview && (
          <Avatar mt={1} boxSize={previewWidth} bg={avtarBg} src={preview} />
        )}
      </Flex>
    </FormControl>
  )
}

export default UserAvatarForm
