import { Box, Stack, StackDivider } from '@chakra-ui/react'
import DeleteListButton from './buttons/DeleteListButton'
import HideCheckedButton from './buttons/HideCheckedButton'

const ListToolbar = ({ showChecked, toggleShowChecked, deleteList, list }) => {
  return (
    <Box>
      <Stack divider={<StackDivider borderColor="gray.200" />}>
        <DeleteListButton deleteList={deleteList} listName={list?.name} />
        <HideCheckedButton
          showChecked={showChecked}
          toggleShowChecked={toggleShowChecked}
        />
      </Stack>
    </Box>
  )
}

export default ListToolbar
