import {
  Checkbox,
  Divider,
  Flex,
  IconButton,
  Stack,
  Text,
  useColorModeValue,
} from '@chakra-ui/react'
import { useRouter } from 'next/router'
import { GrFormEdit } from 'react-icons/gr'

const Item = ({ item, toggleItemChecked, toggleSubitemChecked }) => {
  const router = useRouter()
  const { listId } = router.query
  const hoverBg = useColorModeValue('gray.100', 'gray.900')
  const hoverColor = useColorModeValue('gray.900', 'gray.200')

  return (
    <Stack
      key={item.id}
      _hover={{
        bg: hoverBg,
        color: hoverColor,
      }}
    >
      <Flex direction="row" verticalAlign="center">
        <Checkbox
          isChecked={item.checked}
          marginRight="1em"
          onChange={() => {
            toggleItemChecked(item)
          }}
        />
        <Text
          flexGrow={2}
          as={item.checked ? 's' : undefined}
          onClick={() => router.push(`/lists/${listId}/${item.id}/edit`)}
        >
          {item.title}
        </Text>
        <IconButton
          size="xs"
          aria-label="edit item"
          onClick={() => router.push(`/lists/${listId}/${item.id}/edit`)}
        >
          <GrFormEdit />
        </IconButton>
      </Flex>
      {item.filteredSubitems.length > 0 ? (
        <Stack>
          {item.filteredSubitems.map((subitem) => {
            return (
              <Flex direction="row" paddingLeft="2em" key={subitem.id}>
                <Checkbox
                  isChecked={subitem.checked}
                  marginRight="1em"
                  onChange={() => {
                    toggleSubitemChecked({
                      itemId: item.id,
                      subitemId: subitem.id,
                      checked: subitem.checked,
                    })
                  }}
                />

                <Text as={subitem.checked ? 's' : undefined}>
                  {subitem.title}
                </Text>
              </Flex>
            )
          })}
        </Stack>
      ) : null}
      <Divider />
    </Stack>
  )
}

export default Item
