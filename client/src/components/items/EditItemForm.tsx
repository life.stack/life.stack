import {
  Box,
  Button,
  Checkbox,
  Divider,
  Flex,
  HStack,
  Input,
  InputGroup,
  InputLeftAddon,
  Stack,
  Text,
  Textarea,
  VStack,
} from '@chakra-ui/react'
import { useForm } from 'react-hook-form'
import { GoNote } from 'react-icons/go'
import CreateItem from '../lists/CreateItem'
import { useRouter } from 'next/router'

const EditItemForm = ({
  item,
  updateItem,
  appendSubitem,
  toggleSubitemChecked,
}) => {
  const { register, handleSubmit } = useForm({ defaultValues: item })
  const router = useRouter()
  console.log('EditItemForm', item)

  const onSubmit = async (item) => {
    await updateItem(item)
    await router.back()
  }

  return (
    <VStack>
      <VStack as="form" onSubmit={handleSubmit(onSubmit)}>
        <HStack width="100%" justify="space-between">
          <Button size="xs" colorScheme="gray" onClick={() => router.back()}>
            Back
          </Button>
          <Button size="xs" type="submit" colorScheme="green">
            Save
          </Button>
        </HStack>

        <Input
          {...register('title')}
          placeholder="I want to..."
          aria-label="title of the item"
        />
        <InputGroup>
          <InputLeftAddon>
            <GoNote />
          </InputLeftAddon>
          <Textarea
            {...register('note')}
            placeholder="add a note"
            aria-label="note"
          />
        </InputGroup>
      </VStack>

      <Divider />
      <CreateItem
        appendItem={appendSubitem}
        aria-label="new subitem"
        placeholder="New SubItem"
      />

      <Stack width="full">
        {item?.subitems?.map((subitem) => {
          return (
            <Stack key={subitem.id}>
              <Flex direction="row">
                <Checkbox
                  isChecked={subitem.checked}
                  marginRight="1em"
                  onChange={() => {
                    toggleSubitemChecked(subitem)
                  }}
                />
                <Text as={subitem.checked ? 's' : undefined}>
                  {subitem.title}
                </Text>
              </Flex>
              <Divider />
            </Stack>
          )
        })}
      </Stack>
    </VStack>
  )
}

export default EditItemForm
