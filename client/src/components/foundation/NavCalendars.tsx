import { Collapse, Icon, useDisclosure } from '@chakra-ui/react'
import NavItem from './NavItem'
import { MdKeyboardArrowRight, MdPlaylistAdd } from 'react-icons/md'
import useCalendars from '../../api/useCalendars'
import CalendarsIcon from '../calendar/CalendarsIcon'

const NavCalendars = ({}) => {
  const integrations = useDisclosure()
  const { navBarCalendars } = useCalendars()

  return (
    <>
      <NavItem icon={CalendarsIcon} onClick={integrations.onToggle}>
        Calendars
        <Icon
          as={MdKeyboardArrowRight}
          ml="auto"
          transform={integrations.isOpen && 'rotate(90deg)'}
        />
      </NavItem>
      <Collapse in={integrations.isOpen}>
        <NavItem pl="12" py="2" icon={MdPlaylistAdd} href="/calendars/new">
          Create a new calendar
        </NavItem>
        {Array.isArray(navBarCalendars) &&
          navBarCalendars.map((calendar) => {
            return (
              <NavItem
                key={calendar.id}
                pl="12"
                py="2"
                href={`/calendars/${calendar.id}`}
              >
                {calendar.name}
              </NavItem>
            )
          })}
      </Collapse>
    </>
  )
}

export default NavCalendars
