import {
  Box,
  Collapse,
  Divider,
  Flex,
  Text,
  useColorModeValue,
} from '@chakra-ui/react'
import { SiStackbit as Logo } from 'react-icons/si'
import { MdHome } from 'react-icons/md'
import { AiFillGift } from 'react-icons/ai'
import NavItem from './NavItem'
import Link from 'next/link'
import GroupIcon from '../groups/GroupIcon'
import NavLists from './NavLists'
import NavTrackers from './NavTrackers'
import NavCalendars from './NavCalendars'

const SidebarContent = ({ currentUser, hasGroup, groups, ...props }) => {
  return (
    <Box
      as="nav"
      pos="fixed"
      top="0"
      left="0"
      zIndex="sticky"
      h="full"
      pb="10"
      overflowX="hidden"
      overflowY="auto"
      bg={useColorModeValue('white', 'gray.800')}
      borderColor={useColorModeValue('inherit', 'gray.700')}
      borderRightWidth="1px"
      w="60"
      {...props}
    >
      <Link href="/" passHref>
        <Flex px="4" py="5" align="center">
          <Logo />
          <Text
            fontSize="2xl"
            ml="2"
            color={useColorModeValue('brand.500', 'white')}
            fontWeight="semibold"
          >
            Life.Stack
          </Text>
        </Flex>
      </Link>
      <Flex
        direction="column"
        as="nav"
        fontSize="sm"
        color="gray.600"
        aria-label="Main Navigation"
      >
        <NavItem href="/" icon={MdHome}>
          Home
        </NavItem>
        <Collapse in={hasGroup} unmountOnExit>
          <NavCalendars />
          <NavLists />
          <NavTrackers />
          <NavItem href="/rewards" icon={AiFillGift}>
            Rewards
          </NavItem>
          <Divider />
          <NavItem href="/groups/manage" icon={GroupIcon}>
            Group Management
          </NavItem>
        </Collapse>

        {/*<NavItem href="/settings" icon={BsGearFill}>*/}
        {/*  Settings*/}
        {/*</NavItem>*/}
        {/*{!currentUser && (*/}
        {/*  <NavItem href="/auth/signup" icon={GrUserAdd}>*/}
        {/*    Sign up*/}
        {/*  </NavItem>*/}
        {/*)}*/}
        {/*{!currentUser && (*/}
        {/*  <NavItem href="/auth/signin" icon={GrUser}>*/}
        {/*    Sign in*/}
        {/*  </NavItem>*/}
        {/*)}*/}
        {/*{currentUser && (*/}
        {/*  <NavItem href="/auth/signout" icon={GoSignOut}>*/}
        {/*    Sign Out*/}
        {/*  </NavItem>*/}
        {/*)}*/}
      </Flex>
    </Box>
  )
}

export default SidebarContent
