import {
  Avatar,
  Button,
  Menu,
  MenuButton,
  MenuDivider,
  MenuItem,
  MenuList,
} from '@chakra-ui/react'
import { useRouter } from 'next/router'
import { GrUser } from 'react-icons/gr'
import { useCurrentUserProfile } from '../../api/useCurrentUserProfile'

const UserNav = ({ currentUser }) => {
  const router = useRouter()
  const { userProfile } = useCurrentUserProfile()

  return (
    <Menu>
      <MenuButton as={Button} rounded="full" variant="link" cursor="pointer">
        {currentUser ? (
          <Avatar
            ml="4"
            size="sm"
            name={userProfile?.displayName}
            src={userProfile?.avatar}
            cursor="pointer"
          />
        ) : (
          <Avatar ml="4" size="sm" icon={<GrUser />} cursor="pointer" />
        )}
      </MenuButton>
      <MenuList>
        {!currentUser && (
          <MenuItem onClick={() => router.push('/auth/signin')}>
            Sign in
          </MenuItem>
        )}
        {!currentUser && (
          <MenuItem onClick={() => router.push('/auth/signup')}>
            Sign up
          </MenuItem>
        )}
        {currentUser && (
          <>
            <MenuItem onClick={() => router.push('/profile/currentUser')}>
              Profile
            </MenuItem>
            <MenuDivider />
            <MenuItem onClick={() => router.push('/auth/signout')}>
              Sign out
            </MenuItem>
          </>
        )}
      </MenuList>
    </Menu>
  )
}

export default UserNav
