import {
  Box,
  Button,
  chakra,
  FormControl,
  FormLabel,
  Input,
  Stack,
  useColorModeValue,
} from '@chakra-ui/react'
import React from 'react'
import useGroup from '../../api/useGroup'
import { useUsers } from '../../api/useUsers'
import { useForm } from 'react-hook-form'
import { zodResolver } from '@hookform/resolvers/zod/dist/zod'
import { GroupSchema } from '../../schemas/GroupSchema'
import { DevTool } from '@hookform/devtools'
import { useGroups } from '../../api/useGroups'

const GroupInformation = ({ group, updateGroup }) => {
  const { user } = useUsers()

  const {
    register,
    control,
    handleSubmit,
    formState: { errors },
    setValue,
  } = useForm({
    resolver: zodResolver(GroupSchema),
    defaultValues: group,
  })

  return (
    <chakra.form
      onSubmit={handleSubmit(updateGroup)}
      shadow="base"
      rounded={[null, 'md']}
      overflow={{ sm: 'hidden' }}
    >
      <DevTool control={control} />
      <Stack
        px={4}
        py={5}
        bg={useColorModeValue('white', 'gray.700')}
        spacing={6}
        p={{ sm: 6 }}
      >
        <div>
          <FormControl id="email" mt={1}>
            <FormLabel
              fontSize="sm"
              fontWeight="md"
              color={useColorModeValue('gray.700', 'gray.50')}
            >
              Group Name
            </FormLabel>
            <Input
              {...register('name')}
              shadow="sm"
              focusBorderColor="brand.400"
              fontSize={{ sm: 'sm' }}
            />
          </FormControl>
        </div>
      </Stack>
      <Box
        px={{ base: 4, sm: 6 }}
        py={3}
        bg={useColorModeValue('gray.50', 'gray.900')}
        textAlign="right"
      >
        <Button
          type="submit"
          colorScheme="brand"
          _focus={{ shadow: '' }}
          fontWeight="md"
        >
          Save
        </Button>
      </Box>
    </chakra.form>
  )
}

export default GroupInformation
