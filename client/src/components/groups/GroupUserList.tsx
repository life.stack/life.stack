import {
  Avatar,
  HStack,
  List,
  ListIcon,
  ListItem,
  VStack,
  Text,
  Spacer,
  ButtonGroup,
  Button,
  Badge,
} from '@chakra-ui/react'
import useOtherUserProfile from '../../api/useOtherUserProfile'

const GroupUserList = ({
  groupUsers,
  groupOwners,
  kickUserFromGroup,
  addOwnerToGroup,
}) => {
  return (
    <VStack w="100%">
      {groupUsers.map((userId) => (
        <GroupUserListItem
          key={userId}
          userId={userId}
          groupOwners={groupOwners}
          kickUserFromGroup={kickUserFromGroup}
          addOwnerToGroup={addOwnerToGroup}
        />
      ))}
    </VStack>
  )
}

const GroupUserListItem = ({
  userId,
  groupOwners,
  kickUserFromGroup,
  addOwnerToGroup,
}) => {
  const { userProfile } = useOtherUserProfile(userId)
  const isOwner = groupOwners.includes(userId)
  return (
    <HStack alignItems="center" width="100%">
      <Avatar name={userProfile?.displayName} src={userProfile?.avatar} />
      <VStack alignItems="start">
        <Text>{userProfile?.displayName}</Text>
        <Text fontSize="xs">
          {userProfile?.firstName} {userProfile?.lastName}
        </Text>
      </VStack>
      {isOwner && <Badge colorScheme="green">Owner</Badge>}
      <Spacer />
      <ButtonGroup size="sm">
        {isOwner && <Button disabled>Remove Owner</Button>}
        {!isOwner && (
          <Button onClick={() => addOwnerToGroup({ id: userId })}>Owner</Button>
        )}

        <Button
          colorScheme="red"
          variant="outline"
          onClick={() => kickUserFromGroup({ id: userId })}
        >
          Kick
        </Button>
      </ButtonGroup>
    </HStack>
  )
}

export default GroupUserList
