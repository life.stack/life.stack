import {
  Box,
  Button,
  ButtonGroup,
  chakra,
  Flex,
  IconButton,
  Input,
  InputGroup,
  InputLeftAddon,
  Spacer,
  Stack,
  useClipboard,
  useColorModeValue,
} from '@chakra-ui/react'
import React from 'react'
import useGroup from '../../api/useGroup'
import { HiClipboardCheck } from 'react-icons/hi'
import { useUsers } from '../../api/useUsers'
import GroupUserList from './GroupUserList'
import { isArray } from 'lodash'

const GroupUserManagement = () => {
  const { currentGroup } = useUsers()
  const { group, kickUserFromGroup, addOwnerToGroup } = useGroup(currentGroup)
  console.log('GroupUserManagement', group)

  return (
    <chakra.form
      shadow="base"
      rounded={[null, 'md']}
      overflow={{ sm: 'hidden' }}
    >
      <Stack
        px={4}
        py={5}
        p={[null, 6]}
        bg={useColorModeValue('white', 'gray.700')}
      >
        {isArray(group.users) && group.users.length > 0 && (
          <GroupUserList
            groupUsers={group.users}
            groupOwners={group.owners}
            kickUserFromGroup={kickUserFromGroup}
            addOwnerToGroup={addOwnerToGroup}
          />
        )}
      </Stack>
      <Box
        px={{ base: 4, sm: 6 }}
        py={3}
        bg={useColorModeValue('gray.50', 'gray.900')}
      >
        <ButtonGroup
          as={Flex}
          direction="row"
          alignItems="center"
          justifyContent="space-between"
          verticalAlign="center"
          width="100%"
          size="md"
        >
          <Spacer />
          <Button
            disabled
            h="1.75rem"
            // size="sm"
            colorScheme="red"
            onClick={() => {}}
            marginLeft="2em"
            fontWeight="md"
            _focus={{ shadow: '' }}
          >
            Leave Group
          </Button>
        </ButtonGroup>
      </Box>
    </chakra.form>
  )
}

export default GroupUserManagement
