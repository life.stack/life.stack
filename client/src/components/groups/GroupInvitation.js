import {
  Box,
  Button,
  ButtonGroup,
  chakra,
  Flex,
  IconButton,
  Input,
  InputGroup,
  InputLeftAddon,
  Stack,
  useClipboard,
  useColorModeValue,
} from '@chakra-ui/react'
import React from 'react'
import { useGroups } from '../../api/useGroups'
import { HiClipboardCheck, HiClipboardCopy } from 'react-icons/hi'

const website =
  typeof window !== 'undefined' &&
  ''.concat(window.location.protocol).concat('//').concat(window.location.host)

const GroupInvitation = () => {
  const { generateInvite, inviteCode } = useGroups()
  const { hasCopied, onCopy } = useClipboard(
    `Navigate to ${website} and use the following invite code to join: \n${inviteCode}`
  )

  return (
    <chakra.form
      shadow="base"
      rounded={[null, 'md']}
      overflow={{ sm: 'hidden' }}
    >
      <Stack
        px={4}
        py={5}
        p={[null, 6]}
        bg={useColorModeValue('white', 'gray.700')}
      >
        <InputGroup>
          <InputLeftAddon>Invite Code</InputLeftAddon>
          <Input isReadOnly value={inviteCode} />
        </InputGroup>
      </Stack>
      <Box
        px={{ base: 4, sm: 6 }}
        py={3}
        bg={useColorModeValue('gray.50', 'gray.900')}
      >
        <ButtonGroup
          as={Flex}
          direction="row"
          alignItems="center"
          justifyContent="space-between"
          verticalAlign="center"
          width="100%"
          size="md"
        >
          <IconButton
            // colorScheme="blue"
            aria-label="Copy Invite code"
            icon={hasCopied ? <HiClipboardCheck /> : <HiClipboardCopy />}
            variant="outline"
            // size="md"
            onClick={onCopy}
          >
            Copy to Clipboard
          </IconButton>
          <Button
            h="1.75rem"
            // size="sm"
            colorScheme="brand"
            onClick={generateInvite}
            marginLeft="2em"
            fontWeight="md"
            _focus={{ shadow: '' }}
          >
            Generate
          </Button>
        </ButtonGroup>
      </Box>
    </chakra.form>
  )
}

export default GroupInvitation
