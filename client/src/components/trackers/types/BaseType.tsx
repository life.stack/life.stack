import {
  Button,
  Heading,
  Icon,
  Input,
  Stack,
  Text,
  useColorModeValue,
  VStack,
} from '@chakra-ui/react'
import TrackersIcon from '../TrackersIcon'
import React from 'react'
import { useForm } from 'react-hook-form'
import useTrackers from '../../../api/useTrackers'

const BaseType = () => {
  const { register, handleSubmit } = useForm({
    defaultValues: {
      name: '',
      unit: '',
      type: 'custom',
    },
  })
  const { createTrackerCategory } = useTrackers()

  return (
    <Stack
      boxShadow={'2xl'}
      bg={useColorModeValue('white', 'gray.700')}
      rounded={'xl'}
      p={10}
      spacing={8}
      align={'center'}
    >
      <Icon as={TrackersIcon} w={24} h={24} />
      <Stack align={'center'} spacing={2}>
        <Heading
          textTransform={'uppercase'}
          fontSize={'3xl'}
          color={useColorModeValue('gray.800', 'gray.200')}
        >
          Custom Tracker
        </Heading>
        <Text fontSize={'lg'} color={'gray.500'}>
          Give the tracker a name
        </Text>
      </Stack>
      <Stack
        as="form"
        onSubmit={handleSubmit(createTrackerCategory)}
        spacing={4}
        direction={{ base: 'column', md: 'row' }}
        width="full"
        verticalAlign="center"
      >
        <VStack>
          <Input
            {...register('name')}
            aria-label="give the tracker a name"
            type="text"
            placeholder="Tracker Name"
            color={useColorModeValue('gray.800', 'gray.200')}
            bg={useColorModeValue('gray.100', 'gray.600')}
            rounded="full"
            border={0}
            _focus={{
              bg: useColorModeValue('gray.200', 'gray.800'),
              outline: 'none',
            }}
          />
          <Input
            {...register('unit')}
            aria-label="unit of measure for the tracker"
            type="text"
            placeholder="Unit of Measure"
            color={useColorModeValue('gray.800', 'gray.200')}
            bg={useColorModeValue('gray.100', 'gray.600')}
            rounded="full"
            border={0}
            _focus={{
              bg: useColorModeValue('gray.200', 'gray.800'),
              outline: 'none',
            }}
          />
        </VStack>

        <Button
          type="submit"
          colorScheme="green"
          rounded="full"
          flex="1 0 auto"
          _hover={{ bg: 'blue.500' }}
          _focus={{ bg: 'blue.500' }}
        >
          Create
        </Button>
      </Stack>
    </Stack>
  )
}

export default BaseType
