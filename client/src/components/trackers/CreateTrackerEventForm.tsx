import { Button, Input, Stack } from '@chakra-ui/react'
import { useForm } from 'react-hook-form'
import { format } from 'date-fns'
import { DevTool } from '@hookform/devtools'

const CreateTrackerEventForm = ({ createEventRequest }) => {
  const {
    register,
    handleSubmit,
    // formState: { errors, isDirty, isValid },
    control,
  } = useForm<{ value: string; timestamp: string }>({
    defaultValues: {
      value: '',
      timestamp: format(new Date(), 'yyyy-MM-dd'),
    },
  })

  const onSubmit = ({ value, timestamp }) => {
    createEventRequest({
      value,
      timestamp,
    })
  }

  return (
    <Stack
      as="form"
      onSubmit={handleSubmit(onSubmit)}
      aria-label="track a new event today"
      direction={['column', 'row']}
    >
      <Input
        id="date"
        type="date"
        {...register('timestamp')}
        placeholder={format(new Date(), 'yyyy-MM-dd')}
      />
      <Input
        {...register('value', {
          validate: (value) => value.length > 0 || 'This is required',
        })}
        id="value"
        aria-label="value"
        placeholder="Track Today!"
      />
      <Button type="submit" colorScheme="green">
        Add
      </Button>
      <DevTool control={control} />
    </Stack>
  )
}

export default CreateTrackerEventForm
