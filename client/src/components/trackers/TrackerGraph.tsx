import { PointTooltip, ResponsiveLine, SliceTooltip } from '@nivo/line'
import { Flex, useMediaQuery } from '@chakra-ui/react'
import get from 'lodash/get'

const TrackerGraph = ({ data, dateTickValues, unit, axisBottomFormat }) => {
  const [isLargerThanHD, isDisplayingInBrowser] = useMediaQuery([
    '(min-width: 1920px)',
    '(display-mode: browser)',
  ])

  // const tooltip: PointTooltip = (point) => {
  //   console.log(point)
  //
  //   return point.x
  // }

  return (
    <Flex height={500} width="100%">
      <ResponsiveLine
        data={data}
        curve={'monotoneX'}
        margin={{ top: 40, right: 40, bottom: 60, left: 60 }}
        xScale={{
          format: 'native',
          type: 'time',
          precision: 'day',
        }}
        xFormat="time:%Y-%m-%d"
        axisBottom={{
          format: axisBottomFormat,
          tickValues: dateTickValues,
          legend: 'Date',
          legendOffset: 42,
          legendPosition: 'middle',
        }}
        yScale={{
          type: 'linear',
          min: 'auto',
          max: 'auto',
        }}
        axisLeft={{
          legend: unit,
          legendPosition: 'middle',
          legendOffset: -42,
        }}
        useMesh
        // enableSlices="x"
        // tooltip={tooltip}
      />
    </Flex>
  )
}

export default TrackerGraph
