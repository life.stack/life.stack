import { GiProgression } from 'react-icons/gi'

const TrackersIcon = (props) => {
  return <GiProgression {...props} />
}

export default TrackersIcon
